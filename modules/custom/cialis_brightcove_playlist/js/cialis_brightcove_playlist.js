(function($) {
	Drupal.behaviors.cialis_brightcove_playlist = {
		'attach': function(context, settings) {
			var playlist_videos = settings.cialis_brightcove_playlist.playlist_videos;
			console.log(playlist_videos);

			$.each(playlist_videos, function (screenid, playlist_video) {
				var playlist_id = '';
				var screen_id = screenid;
				$.each(this, function (playlist_index, playlist_video_value) {
					var playlist_videos_html = '';
					if ('playlistid' == playlist_index) {
						playlist_id = playlist_video_value;
					}
					if ('videos' == playlist_index) {
						$.each(this, function (video_index, video_value) {
							playlist_videos_html += '<div class="media video playListVideo">';
							playlist_videos_html += '<a class="BC-openPopin" data-target="bcVideo' + video_value.position + '" data-playlistid="' + playlist_id + '" data-videodescription="' + video_value.description + '" data-videoid="' + video_value.id + '" data-videoposition="' + video_value.position + '" href="#" title="' + video_value.name + '">';
							playlist_videos_html += '<div class="media-left media-middle">';
							playlist_videos_html += '<img class="BC-thumbnail" width="125" src="' + video_value.thumbnail + '" alt="...">';
							playlist_videos_html += '</div>';
							playlist_videos_html += '<div class="media-body">';
							playlist_videos_html += '<span class="BC-title">' + video_value.name + '</span>';
							playlist_videos_html += '</div>';
							playlist_videos_html += '</a>';
							playlist_videos_html += '</div>';
						});
					}
					$("section[data-screen-id='" + screen_id + "'] #playlist-videos").html(playlist_videos_html);
				});
			});
		}
	}
})(jQuery);