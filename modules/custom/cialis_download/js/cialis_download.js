(function($) {
	Drupal.behaviors.cialis_download = {
		'attach': function(context, settings) {
			var i = 1;
			var download_links = '';
			var nb_download_links = settings.cialis_download.nb_download_links;
			for (i = 1; i <= nb_download_links; i++) {
				download_links += '<li class="list-group-item">';
				download_links += '<a target="_blank" class="' + settings.cialis_download.download_links[i].classes + '" data-gtm="' + settings.cialis_download.download_links[i].datagtm + '" href="' + settings.cialis_download.download_links[i].href + '">';
				download_links += settings.cialis_download.download_links[i].text;
				download_links += '</a>';
				download_links += '</li>';
			}
			
			$('ul#donwload-links').html(download_links);
		}
	}
})(jQuery);