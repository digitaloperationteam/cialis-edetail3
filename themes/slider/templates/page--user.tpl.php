<div id="page-wrapper">
 <div id="page">
  <div id="header" class="without-secondary-menu">
   <div class="section clearfix">

    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
    <?php endif; ?>

    <?php if ($site_name || $site_slogan): ?>
      <div id="name-and-slogan">

        <?php if ($site_name): ?>
          <?php if ($title): ?>
            <div id="site-name">
              <strong>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </strong>
            </div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
          <?php endif; ?>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <div id="site-slogan">
            <?php print $site_slogan; ?>
          </div>
        <?php endif; ?>

      </div><!-- /#name-and-slogan -->
    <?php endif; ?>

   </div>
  </div><!-- /.section, /#header -->

  <div id="main-wrapper" class="clearfix">
   <div id="main" class="clearfix">

    <div id="content" class="column">
     <div class="section">
     <?php if ($messages): ?>
        <div id="messages">
         <div class="section clearfix">
          <?php print $messages; ?>
         </div>
        </div><!-- /.section, /#messages -->
      <?php endif; ?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php print render($title_suffix); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>

     </div>
    </div><!-- /.section, /#content -->

   </div>
  </div><!-- /#main, /#main-wrapper -->

  <div id="footer-wrapper">
   <div class="section">

      <div id="footer-columns" class="clearfix">
      </div><!-- /#footer-columns -->

      <!---a class="logo" href="<?php print $front_page; ?>" title=""><img src="<?php print base_path() . path_to_theme(); ?>/images/sanofi-pasteur-msd.jpg?<?php print time(); ?>" alt="" width="150"></a-->

   </div>
  </div><!-- /.section, /#footer-wrapper -->

 </div>
</div><!-- /#page, /#page-wrapper -->
