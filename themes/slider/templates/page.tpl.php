<div id="page-wrapper">
<div class="container-fluid">      

    <nav class="navbar navbar-fixed-top">
          <div class="container">
            <div class="logo">
                <img src="<?php print base_path() . path_to_theme(); ?>/dist/images/<?php echo $language->language; ?>/logo.png" />
            </div> 
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="glyphicon glyphicon-menu-hamburger"></span>
              </button>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <div id="lilly"><img class="onlyUK" src="<?php print base_path() . path_to_theme(); ?>/dist/images/<?php echo $language->language; ?>/lilly.png" /></div>
                <!-- ul class="nav navbar-nav">
                    <li role="presentation"><a href="#"><?php print t('Terms and conditions'); ?></a></li>
                    <li role="presentation"><a href="#" data-toggle="modal" data-target="#myModal"><?php print t('References'); ?></a></li>
                    <li role="presentation"><a href="#"><?php print t('Prescribing Information'); ?></a></li>
                    <li role="presentation"><a href="#"><?php print t('Privacy'); ?></a></li>
                    <li role="presentation"><a href="#"><?php print t('Cookies'); ?></a></li>
                    <li role="presentation"><a href="#"><?php print t('Disclaimer'); ?></a></li>
                </ul-->
                <?php if ($page['navigation']) : ?>
                	<?php print render($page['navigation']); ?>
                <?php endif; ?>

            </div>

          </div>
    </nav>
    <div id="fullpage">
	<?php echo $slides;	?>
	</div>

</div>
</div>
