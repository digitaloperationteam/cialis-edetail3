<section id="productdesc_10-1" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s013" data-screen-label="Product description" data-screen-name="Product description" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">Two problems, one treatment. </span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
              <h2 data-i18n="screen_title">Two problems, one treatment<br><span>Cialis 5 mg Once-Daily<sup>7</sup></span></h2>
            </div>
            <div class="row">
                <div class="col-xs-8 col-md-6">
                  <ul class="bubbleList">
                    <li>
                        <span data-i18n="list_item1">If your patient has ED, consider asking them about LUTS-BPH, and vice-versa<sup>1,2</sup></span>
                    </li>
                    <li>
                        <span data-i18n="list_item2">ED and LUTS-BPH co-exist in many men<sup>1</sup></span>
                    </li>
                    <li>
                        <span data-i18n="list_item3">Cialis 5 mg Once-Daily has been shown to significantly improve both ED and LUTS-BPH<sup>8</sup></span>
                    </li>
                    <li>
                        <span data-i18n="list_item4">With Cialis 5 mg Once-Daily, couples regain their sexual quality of life<sup>9</sup></span>
                    </li>
                  </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-8 col-md-7">
                  <p>
                    <strong>CIALIS 5 mg therapeutic indications:<sup>6</sup></strong><br>
                    Treatment of erectile dysfunction in adult males. In order for tadalafil to be effective for the treatment of erectile dysfunction, sexual stimulation is required.<br>
                    Treatment of the sign and symptoms of benign prostatic hyperplasia in adult males.
                  </p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-8 col-md-7">
                  <p class="orange">
                    For contraindications, warnings, precautions, side effects, and adverse event reporting, please see the prescribing information below
                  </p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#pi_14" data-i18n="button">Prescribing Information <span class="caret"></span></button>
                </div>
            </div>
                
        </div> 
    </div>

    <div class="paralaxElements">
    </div>

</section>