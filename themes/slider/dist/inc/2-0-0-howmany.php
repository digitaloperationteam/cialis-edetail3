<section id="howmany_02" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s003" data-screen-label="How many men like Barry have these problems?" data-screen-name="How many men like Barry have these problems?" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">How many men like Barry have these problems?</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">How many men like Barry<br><span>have these problems?</span></h2>
                <div class="blc">
                    <h3 data-i18n="quizz_title">What percentage of men with ED also have LUTS-BPH?*</h3>
                   
                    <div id="slider"></div>
                    <div class="indicator onlyUK"><span></span></div>
                    <div id="dragItem">
                        <span id="men_green" class="men"></span>
                    </div>   
                    <button class="showAnswer gtmClickQuizShowAnswer" data-gtm="ShowQuizAnswer" type="submit" data-i18n="quizz_submit_button">Submit</button>
                    <p id="quizz_answer" data-i18n="quizz_answer">
                      The incidence and severity of both LUTS and ED increase with age, particularly in men over 50.<sup>2</sup>
                    </p>
                </div>
                <p class="legend" data-i18n="legend">*Lower Urinary Tract Symptoms associated with Benign Prostatic Hyperplasia</p>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#specialist_03" data-i18n="button">What do experts have to say? <span class="caret"></span></button>
                </div>
            </div>
        </div>
    </div>
    <div class="paralaxElements">
        <span class="hidden" data-i18n="orange_bubble">Erection problems and trouble using the bathroom - is this just part of growing older?</span>
    </div>
</section>