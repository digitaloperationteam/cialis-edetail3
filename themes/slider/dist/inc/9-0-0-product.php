<section id="product_11" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s014" data-screen-label="Help patients like Barry regain their previous lifestyle" data-screen-name="Help patients like Barry regain their previous lifestyle" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">Help patients like Barry regain their previous lifestyle...</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">Help patients like Barry<br><span>regain their previous lifestyle...</span></h2>
                <h3 data-i18n="screen_subtitle">Cialis 5 mg Once Daily<br>Two problems, one treatment for ED and LUTS-BPH:<sub>7</sub></h3>
            </div>
            <div class="row">
              <div class="infos-products">
                <div class="col-xs-12 col-sm-6">
                    <img src="img/product-5mg.png" alt="" class="img-responsive">
                </div>
                <div class="v-center col-xs-12 col-sm-6">
                    <ul>
                      <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <span data-i18n="list_item1">To help patients with ED reclaim sexual spontaneity with their partners<sup>8,12</sup></span><br>
                      <span data-i18n="list_item1_content">Sexual stimulation is required in order for Cialis to be effective in treating erectile dysfunction.<sup>7</sup></span></li>
                      <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <span data-i18n="list_item2">To relieve urinary discomfort associated with LUTS-BPH<sup>13,16</sup></span><br><span data-i18n="list_item2_content">Indicated for the treatment of the signs and symptoms of benign prostatic hyperplasia in adult men.<sup>7</sup></span></li>
                    </ul>
                </div>
              </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
    </div>
    
</section>