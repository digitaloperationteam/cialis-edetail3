<section id="graph_08" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s010" data-screen-label="Cialis benefits in ED and LUTS-BPH" data-screen-name="Cialis benefits in ED and LUTS-BPH" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">Cialis benefits in ED and LUTS-BPH</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">Cialis benefits in<br><span>ED and LUTS-BPH</span></h2>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-push-4">
                  <img class="img-responsive" src="images/content-graph-06.png" width="574" height="424">
                </div>
                <div class="v-center col-xs-12 col-md-4 col-md-pull-8">
                    <span class="round-icon check"></span>
                    <p class="text-center" data-i18n="graph_legend">Cialis 5 mg Once Daily improves subjects' and partners' sexual QoL, with comparable treatment satisfaction to on-demand ED treatments<sup>8,9</sup></p>
                    <div class="fishbone">
                          <p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#graph_09" data-i18n="button">And for LUTS-BPH... <span class="caret"></span></button>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
      
    </div>
</section>