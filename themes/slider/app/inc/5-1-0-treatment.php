<section id="treatment_06-1" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s008" data-screen-label="Significantly improved erectile function versus placebo" data-screen-name="Significantly improved erectile function versus placebo" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">Significantly improved erectile function versus placebo</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">Cialis 5 mg Once-Daily<br><span>Significantly improved<br> erectile function</span><br>versus placebo, measured by IIEF-EF* at 12 weeks<sup>8</sup></h2>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-6 col-md-offset-2">
                    <img class="img-responsive" src="images/content-graph-08.png" width="580" height="307">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p class="legend">Adapted from Egerdie RB at al 2012<br>
                    *The international Index of Erectile Function (IIEF) is a self-administered, 4 weeks recall questionnaire used to assess the presence and severity of ED in men.<br>
                    &#8224;P&lt;0.001 vs placebo</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#how_07" data-i18n="button">For ED & Sexual Quality of Life <span class="caret"></span></button>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
    </div>
    
</section>