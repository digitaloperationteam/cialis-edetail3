<section id="download_13" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s015" data-screen-label="A diagnosis tool kit to help you help Barry" data-screen-name="A diagnosis tool kit to help you help Barry" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">A diagnosis tool kit to help you help Barry</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">A diagnosis tool kit <br><span>to help you help Barry</span></h2>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-5 no-col-sm">
                  <div class="blc">
                     <h3 data-i18n="downloads_title">Downloads</h3>
                     <ul id="donwload-links" class="list-group">
                     </ul>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-5 no-col-sm">
                  <div class="blc">
                     <h3 data-i18n="videos_titl">Video clips</h3>
                     <div id="playlist-videos">
                     </div>
                  </div>
              </div>
              
              
            </div>
            <div id="footer" class="row itOnly">
              <div class="col-sm-12">
                <p class="mentions text-center">
                  ITCLS00991 data di deposito AIFA 03/08/2017
                </p>
              </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
    </div>

</section>