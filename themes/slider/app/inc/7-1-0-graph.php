<section id="graph_09" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s011" data-screen-label="A conversation tool kit to help you help Barry" data-screen-name="A conversation tool kit to help you help Barry" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">Cialis benefits in ED and LUTS-BPH 2/2</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row onlyUK">
                <h2 data-i18n="screen_title">Cialis 5 mg Once-Daily <br><span>Benefits for LUTS-BPH</span></h2>
            </div>
            <div class="row">
                <div class="v-center col-md-4">
                    <div class="fishbone">
                          <p>&nbsp;</p>
                    </div>
                    <span class="round-icon check"></span>
                    <p class="text-center" data-i18n="graph_legend">Cialis 5 mg Once Daily can cause a significant improvement in his LUTS-BPH as measured by total International Prostate Symptom Score (IPSS) -similar efficacy to tamsulosin 0.4&nbsp;mg<sup>10</sup></p>
                </div>
                <div class="col-md-8">
                  <img class="img-responsive" src="images/content-graph-07.png" width="574" height="489">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#timeline_10" data-i18n="button">How long will it take to work? <span class="caret"></span></button>
                </div>
            </div>

        </div> 
    </div>
    <div class="paralaxElements">
    </div>
</section>