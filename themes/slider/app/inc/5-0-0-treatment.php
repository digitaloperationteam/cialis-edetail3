<section id="treatment_06" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s007" data-screen-label="One treatment for both Barry's problems" data-screen-name="One treatment for both Barry's problems" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">One treatment for both Barry's problems</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8">
                    <h2 data-i18n="screen_title"><span>One treatment for both</span><br>Barry's problems</h2>
                    <p data-i18n="screen_subtitle">Cialis 5 mg Once Daily is indicated for both ED and LUTS-BPH.<sup>7</sup></p>
                </div> 
            </div>
            <div class="row">
                <div id="quoteBlock" class="whiteBox col-sm-12 col-md-7">
                    <p class="text-center" data-i18n="quote_source">The 2016 European Association of Urology Guidelines on the management of LUTS-BPH state that:<sup>3</sup></p>
                    <blockquote data-i18n="quote">"Although clinical trials of several selective oral PDE5 [inhibitors] have been conducted in men with LUTS, only [Cialis] tadalafil (5 mg once daily) has been licensed for the treatment of male LUTS."</blockquote>  
                    <span class="round-icon talk"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#how_07" data-i18n="button">How does Cialis work in LUTS-BPH? <span class="caret"></span></button>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
        <span class="hidden" data-i18n="yellow_bubble">I don't want to have to take another pill.</span>
    </div>
    
</section>