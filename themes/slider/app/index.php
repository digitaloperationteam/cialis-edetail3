<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Cialis - (tadalafil) tablets</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--build:remove -->

        <!-- endbuild -->

        <!--build:css css/styles.min.css-->
        <link rel="stylesheet" href="css/main.css"> 
        <!--endbuild-->
    </head> 
    <body>
        
    <div class="container-fluid">      

    <nav class="navbar navbar-fixed-top">
          <div class="container">
            <div class="logo">
                <img src="images/logo.png" />
            </div> 
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="glyphicon glyphicon-menu-hamburger"></span>
              </button>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li role="presentation"><a href="#">terms and conditions</a></li>
                    <li role="presentation"><a href="#">references</a></li>
                    <li role="presentation"><a href="#">Prescribing Information</a></li>
                    <li role="presentation"><a href="#">Privacy</a></li>
                    <li role="presentation"><a href="#">cookies</a></li>
                    <li role="presentation"><a href="#">disclaimer</a></li>
                </ul>
            </div>
          </div>
    </nav>
    <div id="fullpage">
                <?php

                    function recursiveDirectoryInclude($dir) {
                        $cdir = scandir($dir);
                        foreach ($cdir as $key => $value) {
                         if (!in_array($value, array('.', '..', '.DS_Store', '._.DS_Store'))) {
                          if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
                            recursiveDirectoryInclude($dir . DIRECTORY_SEPARATOR . $value);
                          }
                          else {
                              include $dir . DIRECTORY_SEPARATOR . $value;
                          }
                         }
                        }
                    }

                    recursiveDirectoryInclude('inc');
                ?>
    </div>
    </div>
        
        <!--build:remove -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../../_player/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.js"></script> 
        <!-- endbuild -->

        <!--build:js js/main.min.js -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.parallax-1.1.3.js"></script>
        <script src="js/jquery.transform2d.js"></script>  
        <script src="js/scrollNav.js"></script> 
        <script src="js/main.js"></script> 
        <!-- endbuild -->
        
        <script>

        </script>

    </body>
</html>
