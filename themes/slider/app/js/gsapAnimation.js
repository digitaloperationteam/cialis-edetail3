(function ($) {
    
    function gsapAnimation (origin, target, duration, animationType, delayTime, staggerTime){
        if(!delayTime){ delayTime = 0; }
        if(!staggerTime){ staggerTime = 0; }
        
        var vars;
        switch(animationType) {
            case "fadeIn":
                vars = {opacity:0, delay:delayTime}
                break;
            case "fadeOut":
                vars = {opacity:1, delay:delayTime}
                break;
            case "slideFromLeft":
                vars = {opacity:0, x:-1000, ease:Power2.easeOut, delay:delayTime}
                break;
            case "slideFromRight":
                vars = {x:1000, ease:Power2.easeOut, delay:delayTime}
                break;
            case "slideFromTop":
                vars = {opacity:0, y:-1000, ease:Power2.easeOut, delay:delayTime}
                break;
            case "slideFromMiddleTop":
                vars = {opacity:0, y:-50, ease:Power2.easeOut, delay:delayTime}
                break;
            case "slideFromBottom":
                vars = {opacity:0, y:1000, ease:Power2.easeOut, delay:delayTime}
                break;
            case "zoomIn":
                vars = {opacity:0, scale:0, ease:Power2.easeOut, delay:delayTime}
                break;
            case "zoomInOut":
                vars = {opacity:0, scale:0, ease:Back.easeInOut.config(1.7), delay:delayTime}
                break;
            case "bounce":
                vars = {scale:0, ease: Bounce.easeOut, delay:delayTime}
                break;
            case "rotation":
                vars = {rotation:360, repeat:1, ease:Power0.easeNone, delay:delayTime}
                break;
            case "infiniteRotation":
                vars = {rotation:360, repeat:-1, ease:Power0.easeNone, delay:delayTime}
                break;
            case "slowWiggle":
                vars = {scale:0.95, yoyo:true, repeat:-1, ease:Bounce.easeInOut, delay:delayTime}
                break;
            case "borderWhite":
                vars = {css:{borderRight:'2px solid rgba(255, 255, 255, 0)'},ease:Power2.easeOut, delay:delayTime}
                break;
            case "borderBlack":
                vars = {css:{borderRight:'2px solid rgba(255, 255, 255, 0)'},ease:Power2.easeOut, delay:delayTime}
                break;
            case "customBounce":
                vars = CustomBounce.create("myBounce", {strength:0.7, squash:3});
        }
        switch(origin) {
            case "from":
                TweenMax.from(target, duration, vars);
                break;
            case "staggerFrom":
                TweenMax.staggerFrom(target, duration, vars, staggerTime);
                break;
        }
        
    }
    
    jQuery(window).on('slideChange', function (e) {
        if(jQuery('#emotional_04').hasClass('active') && !jQuery('#emotional_04').hasClass('animationDone')){
            
            gsapAnimation('staggerFrom', jQuery('#emotional_04').find('.slideFromRight'), 2, 'slideFromRight', 0, .2);
            gsapAnimation('staggerFrom', jQuery('#emotional_04').find('.slideFromLeft'), 2, 'slideFromLeft', 0, .2);
            TweenMax.to(jQuery('#emotional_04').find('.button'), 1, {opacity:1});
            TweenMax.to(jQuery('#emotional_04').find('.circle'), 1, {opacity:1});

            jQuery('#emotional_04').addClass('animationDone');
        }else if(jQuery('#men_05').hasClass('active') && !jQuery('#men_05').hasClass('animationDone')){
            gsapAnimation('staggerFrom', jQuery('#men_05').find('.slideFromRight'), 2, 'slideFromRight', 0, .2);
            gsapAnimation('staggerFrom', jQuery('#men_05').find('.slideFromLeft'), 2, 'slideFromLeft', 0, .2);
            TweenMax.to(jQuery('#men_05').find('.circle'), 1, {opacity:1});

            jQuery('#men_05').addClass('animationDone');
        }else if(jQuery('#specialist_03').hasClass('active') && !jQuery('#specialist_03').hasClass('animationDone')){
            var tl = new TimelineLite();
            var elem1 = jQuery('#specialist_03').find('.bulle:nth-child(1)'),
                elem2 = jQuery('#specialist_03').find('.bulle:nth-child(2)'),
                elem3 = jQuery('#specialist_03').find('.bulle:nth-child(3)');
            tl.from(elem1, 1, {left:100, opacity:0})
                .from(elem1, 1, {top:200})
                .from(elem2, 1, {left:100, opacity:0})
                .from(elem2, 1, {top:100})
                .from(elem3, 1, {left:100, opacity:0})
        
            //TweenMax.to(jQuery('#specialist_03').find('.bulle:nth-child(1)'), 1, {display:'block'});
            //gsapAnimation('staggerFrom', jQuery('#specialist_03').find('.bulle:nth-child(1), .bulle:nth-child(2), .bulle:nth-child(3)'), 2, 'slideFromLeft', 0, .2);
            jQuery('#specialist_03').addClass('animationDone');

        } else if(jQuery('#timeline_10').hasClass("active") && !jQuery("#timeline_10").hasClass('animationDone')){
            var tl = new TimelineLite();
            var elem1 = jQuery('#timeline_10').find('.leftTopBubble'),
                elem2 = jQuery('#timeline_10').find('.rightTopBubble'),
                elem3 = jQuery('#timeline_10').find('.leftBottomBubble'),
                elem4 = jQuery('#timeline_10').find('.rightBottomBubble');
            tl.from(elem1, 1, {left:300, opacity:0})
                .from(elem2, 1, {right:300, opacity:0})
                .from(elem3, 1, {left:100, opacity:0})
                .from(elem4, 1, {right:100, opacity:0})
            //gsapAnimation('staggerFrom', jQuery('#timeline_10').find('.leftTopBubble'), 2, 'slideFromLeft', 0, .2);
            //gsapAnimation('staggerFrom', jQuery('#timeline_10').find('.rightTopBubble'), 2, 'slideFromRight', 0, .2);
            //gsapAnimation('staggerFrom', jQuery('#timeline_10').find('.leftBottomBubble'), 2, 'slideFromLeft', 0, .2);
            //gsapAnimation('staggerFrom', jQuery('#timeline_10').find('.rightBottomBubble'), 2, 'slideFromRight', 0, .2);
            jQuery('#timeline_10').addClass('animationDone');
        }
    });
    
    
})(jQuery);