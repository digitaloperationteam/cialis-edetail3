/*****************************
        PARALLAX CONFIG
*****************************/
jQuery(document).ready(function(){    
    jQuery('.orangeBubble').parallax("50%", 0.15);
    jQuery('.greyBubble').parallax("50%", 0.3);

    /********** ENG-UK SPECIFIC *********/
    jQuery('.i18n-eng-uk #howmany_02 button.scrollToSection').attr('href','#diagnose_05');
    jQuery('.i18n-eng-uk #diagnose_05 button.scrollToSection').attr('href','#treatment_06');
    jQuery('.i18n-eng-uk #diagnose_05 button.scrollToSection').attr('href','#treatment_06');
    jQuery('.i18n-eng-uk #treatment_06 button.scrollToSection').attr('href','#treatment_06-1');
    jQuery('.i18n-eng-uk #treatment_06-1 button.scrollToSection').attr('href','#graph_08');
    jQuery('.i18n-eng-uk #timeline_10 button.scrollToSection').attr('href','#productdesc_10-1');
    jQuery("#howmany_02 .indicator").css("width", jQuery("#howmany_02 #slider").width());

    /********** ITA-IT SPECIFIC *********/
    jQuery('.i18n-ita-it #howmany_02 button.scrollToSection').attr('href','#specialist_04');
});


/********** QUIZZ *********/
jQuery('.quizz button').on('click', function(){
    jQuery(this).parent().addClass('showAnswer');
});

// drag system start
jQuery(function () {
    /*jQuery("#dragItem").draggable({
        opacity: 0.5
    });*/
    // faire un tableau associatif des img
    jQuery("#slider").slider({

        range: "min",
        value: 0,
        min: -1,
        max: 101,
        step: 1,
        /*slide: function (event, ui) {
        	jQuery("#men_green").css('opacity', 0+ui.value*.01); //trouver le multiplicateur pour boost opacité
        }*/
    });
    

    function rangeMenSteps(progress){
        
        var menSlider = jQuery("#dragItem>#men_green");
        
        if(progress>0){
            menSlider.css({
                "opacity" : 1,
                "width" : progress + "px"
            });
        } else {
            menSlider.css({
                "opacity" : 0,
                "width" : "0px"
            });
        }
    }

    function setIndicatorUK(progress){
        
        var indicator = jQuery(".indicator>span");
        if(progress>0){
            indicator.show();
            indicator.css({
                "display" : "block",
                "padding-left" : progress + "%"
            });
            indicator.append(progress + '%<br>');
        }
    }

    function setRange(fromElem){
        
        var progress = fromElem.width() / fromElem.parent().width() * 100;
        var slideRange = jQuery("#slider .ui-slider-range");
        slideRange.css("width", progress + "%");
    }

    function setRangeValue(value){
        
        var handleValue = jQuery(".ui-slider-handle");
        handleValue.empty().append("<span>" + value + "</span>");
        handleValue.css("left", value + "%");
    }

    
    var slider = jQuery("#slider");
    var submit = jQuery("#howmany_02 button.showAnswer");
    var userValue;
    var finalValue = 72;

    slider.on("slide", function(){
        setRangeValue(slider.slider("value"));
        userValue = slider.slider("value");
    })
    slider.on("slidestop", function(){
        var rangeProgress = jQuery("#slider .ui-slider-range").width();
        rangeMenSteps(rangeProgress); 
    })

    submit.on("click", function(){
        jQuery(this).hide();
        jQuery(this).parent().find('p#quizz_answer').show();
        rangeMenSteps(slider.width()*finalValue/100);
        setIndicatorUK(userValue);
        setRange(jQuery("#dragItem>#men_green"));
        setRangeValue(""+finalValue+"");
        jQuery(".ui-slider-handle").addClass('onresults');
        jQuery("span.ui-slider-handle.onresults").css("background-position", "top center");
        slider.slider( "disable" );
    })

});

/*********************************
    Doyghnut chart - Chart.js
*********************************/

function createDoughnutChart(ctx, data, color1, color2, cutoutPercentage){

    ctx.addClass('created');
    var data = {
    labels: ["","",""],
    datasets: [
        {
            data: data,
            backgroundColor: [
                color1,
                color2
            ],
            borderWidth:0
        }]
};
// And for a doughnut chart
var myDoughnutChart = new Chart(ctx, {
    type: 'doughnut',
    data: data,
    options: {
        legend: {
            display: false
        },
        hover : {
            display: false
        },
        cutoutPercentage : cutoutPercentage
    }
});
}



jQuery(window).on('slideChange', function (e) {
    //Charts
    var ctx = jQuery("#lightgreenChart");
    if(jQuery('#specialist_04').hasClass('active') && !ctx.hasClass('created')){
        var greenData = [62, 38],
            greenColor1 = '#96d465',
            greenColor2 = 'transparent';
        createDoughnutChart(jQuery("#lightgreenChart"),greenData, greenColor1, greenColor2, 70);
    } 
    var ctx = jQuery("#mediumgreenChart");
    if(jQuery('#specialist_04').hasClass('active') && !ctx.hasClass('created')){
        var greenData = [27, 73],
            greenColor1 = '#6fab40',
            greenColor2 = 'transparent';
        createDoughnutChart(jQuery("#mediumgreenChart"),greenData, greenColor1, greenColor2, 65);
    } 
    var ctx = jQuery("#darkgreenChart");
    if(jQuery('#specialist_04').hasClass('active') && !ctx.hasClass('created')){
        var greenData = [19, 81],
            greenColor1 = '#457c1f',
            greenColor2 = 'transparent';
        createDoughnutChart(jQuery("#darkgreenChart"),greenData, greenColor1, greenColor2, 60);
    } 

    

});

