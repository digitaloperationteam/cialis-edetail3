/*********
    AUTHOR: LOPEZ Pierre-Yves
    Dependecies:
    * - videosjs-overlay.js

    //ON THE HTML PAGE : //
    var videos = {
        apiBaseURL : 'https://edge.api.brightcove.com/playback/v1',
        data: {
            account : '4931690864001',
            player : 'default',
            embed : 'default',
            policy_key : '',
            playlist_id : '4941468076001'
        }
    } 
*********/

(function($){

  $(document).ready(function() {
    //ONLY IF INIT IN THE PAGE
      //GLOBAL VAR
      var arrayVideos = [],
          currentPosition;

      var apiBaseURL = '',
          // element references
          account_id = '',
          playlist_id = '';
        
      $('.playListVideo .BC-openPopin').on('click', showOverlayVideo);

      /***********************************
      ************* OVERLAYS *************
      ************************************/ 

      function videoHml5Listener(){
        var ua = navigator.userAgent;
        if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)){
          hideLoadingOverlay();
          $('#overlayVideo').fadeTo('slow', '1');
          document.getElementById('overlayVideo').style.opacity = '1';
          $('body').addClass('BC-overlay');
          overlayAddEventListener();
        }else if(ua.match('MSIE')|| ua.match('Trident/')){
          //Fix for IE
            hideLoadingOverlay();
            $('#overlayVideo').fadeTo('slow', '1');
            document.getElementById('overlayVideo').style.opacity = '1';
            $('body').addClass('BC-overlay');
            overlayAddEventListener();
        }else{
          $('video').on('canplay', function (e) {
            hideLoadingOverlay();
            $('#overlayVideo').fadeTo('slow', '1');
            document.getElementById('overlayVideo').style.opacity = '1';
            $('body').addClass('BC-overlay');
              //videojsOverlay();
            overlayAddEventListener();
          });

          $('video').on('play', function (e) {
              //hideSuggestBloc();
          });

          $('video').on('pause', function (e) {
              //showSuggestBloc();
          });
        }    
      }

      function overlayAddEventListener(){
         //Add event listener
          document.body.addEventListener("click", hideOverlayVideo);
          document.getElementById("closeBtn").addEventListener("click", hideOverlayVideo);
          document.body.addEventListener("touchend", hideOverlayVideo);
          document.getElementById("closeBtn").addEventListener("touchend", hideOverlayVideo);
          document.getElementById("BC-overlayVideo").addEventListener("click", hideOverlayVideo);
          document.getElementById("BC-contentOverlay").addEventListener("click", function(e){e.stopPropagation()});
          document.getElementById("BC-overlayVideo").addEventListener("touchend", hideOverlayVideo);
          document.getElementById("BC-contentOverlay").addEventListener("touchend", function(e){e.stopPropagation()});
          document.addEventListener("keyup", function(e){ if(e.keyCode == 27) hideOverlayVideo(); }, false);
      }   
      function overlayRemoveEventListener(){
         //Add event listener
         document.body.removeEventListener("click", hideOverlayVideo);
         document.getElementById("closeBtn").addEventListener("click", hideOverlayVideo);
         document.body.removeEventListener("touchend", hideOverlayVideo);
         document.getElementById("closeBtn").addEventListener("touchend", hideOverlayVideo);
          document.getElementById("BC-overlayVideo").removeEventListener("click", hideOverlayVideo);
          document.getElementById("BC-contentOverlay").removeEventListener("click", function(e){e.stopPropagation()});
          document.getElementById("BC-overlayVideo").removeEventListener("touchend", hideOverlayVideo);
          document.getElementById("BC-contentOverlay").removeEventListener("touchend", function(e){e.stopPropagation()});
          document.removeEventListener("keyup", function(e){ if(e.keyCode == 27) hideOverlayVideo(); }, false);
      }     

      function initPlayer(videoID, videoTitle, videoDescription){
          if(!currentPosition){ var currentPosition = 0;};
          
          if(document.getElementById("BC-overlayVideo") == null){ 
          var overlay = document.createElement("div");
                  overlay.id = 'BC-overlayVideo';
                  overlay.style.opacity = '0';
          var contentOverlay = document.createElement("div");
              contentOverlay.id = 'BC-contentOverlay';

                  overlay.appendChild(contentOverlay);
                  $('body').append(overlay); 
          }
          $.getScript('http://players.brightcove.net/4931690864001/rJSbLDiJe_default/index.min.js', function() {
          //Reloading the js
           });

          var videoHTML = '<div class="BC-iframeContainer"><div style="display: block; position: relative; max-width: 100%;"><div style="padding-top: 56.25%;"><video id="BC-playerHTML5_html5_api" data-video-id="'+videoID+'" data-account="4931690864001" data-player="rJSbLDiJe" data-embed="default" class="video-js" controls style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px; opacity:1;"></video></div></div></div>';

          var videoDescription = '<div class="descriptionBC"><h3>'+videoTitle+'</h3><p>'+ videoDescription +'</p></div>';


          $('#BC-contentOverlay').append(videoHTML);
          $('#BC-contentOverlay').append(videoDescription);
          
      }

      function hideOverlayVideo(){
          var player = videojs("BC-playerHTML5_html5_api"); 
          overlayRemoveEventListener();
          $('#overlayVideo').fadeTo('slow', '0', function(){
              $('#BC-contentOverlay').empty();
          });
          $('body').removeClass('BC-overlay');
      }
          
      function showOverlayVideo(){
          $('#BC-contentOverlay').empty();
 
          var videoID = $(this).attr('data-videoid'),
              videoTitle = $(this).attr("title"),
              videoDescription = $(this).attr('data-videodescription');
              closeBtn = document.createElement("a");
              closeBtn.id = "closeBtn";
              closeBtn.class = "closeBtn";

          document.getElementById('BC-contentOverlay').appendChild(closeBtn);
          currentPosition = $(this).attr('data-videoPosition');
           showLoadingOverlay();
           initPlayer(videoID, videoTitle, videoDescription);
           videoHml5Listener();
          return false;
        
      }

      function showLoadingOverlay(){
          var overlay = document.createElement('div'),
              spiner = document.createElement('div')
              bounce1 = document.createElement('div');
              bounce2 = document.createElement('div');

              overlay.setAttribute('class', 'BC-overlayLoading');
              spiner.setAttribute('class', 'spinner');
              bounce1.setAttribute('class', 'double-bounce1');
              bounce2.setAttribute('class', 'double-bounce2');

              overlay.appendChild(spiner);
              spiner.appendChild(bounce1);
              spiner.appendChild(bounce2);

            $('body').append(overlay);
      }

      function hideLoadingOverlay(){
          $('.BC-overlayLoading').fadeTo('slow', '0');
          $('.BC-overlayLoading').remove();
      }
  });
})(jQuery)
