/**********************
Author: LOPEZ Pierre-Yves
Company: Aptus Health
**********************/

var slides = jQuery('section.slides');

//CustomEvent
var slideChange = new CustomEvent('slideChange');

// Init :
jQuery(document).ready(function(){
    slides.first().addClass('active');
    createPathhNav();
});

//Event listener :
jQuery('.btn-next').on('click', function(){
	scrollToNext();
});

jQuery('.btn-prev').on('click', function(){
    scrollToPrev();
});

jQuery('.scrollToSection').on('click', function(){
    sectionId = jQuery(this).attr('href');
    scrollToSection(sectionId);
});

jQuery(window).on('slideChange', function (e) {
    changeCurrentPathNav();
   
    var sectionId =  jQuery('section.slides.active').attr('id');
});

/* handle the mousewheel event together with 
 DOMMouseScroll to work on cross browser */
jQuery(document).on('scroll mousewheel DOMMouseScroll swipedown swipeup', function (e) {
    //e.preventDefault();//prevent the default mousewheel scrolling
    var active =  jQuery('section.active'),
        newSectionActive = ''; 
    //get the delta to determine the mousewheel scrol UP and DOWN
    slides.each(function(){
        var sectionPosition =jQuery(this).offset().top, 
            scrollPosition = jQuery(window).scrollTop(),
            offsetScrollPosition = jQuery(window).height() + jQuery(window).scrollTop();
        offsetScrollPosition = offsetScrollPosition - (offsetScrollPosition * 0.05);
        if(offsetScrollPosition >= sectionPosition){
            if(!jQuery(this).hasClass('active')){
                slides.removeClass('active');
                jQuery(this).addClass('active');
                newSectionActive = jQuery(this);
            }
        }
    });
    if(newSectionActive){ 
        if(active.attr('id') != newSectionActive.attr('id')){
            window.dispatchEvent(slideChange);
        }
    }
});
 
// Functions : 
function scrollToPrev(){
    var currentSlide = jQuery('section.slides.active'),
        nextSlide = currentSlide.next(),
        previousSlide = currentSlide.prev();

    //Check if there is a previous slide
    if (previousSlide.length) {
        jQuery('body, html').animate({
            scrollTop: previousSlide.offset().top
        }, 'slow');

        previousSlide.addClass('active').siblings().removeClass('active');
        window.dispatchEvent(slideChange);
    }
}

function scrollToNext(){
    var currentSlide = jQuery('section.slides.active'),
        nextSlide = currentSlide.next(),
        previousSlide = currentSlide.prev();

    //Check if there is a next slide
    if (nextSlide.length) {
            jQuery('body, html').animate({
                scrollTop: nextSlide.offset().top
            }, 'slow');

            nextSlide.addClass('active').siblings().removeClass('active');
            window.dispatchEvent(slideChange);
    }
}

function scrollToSection(sectionID){
    newSection = jQuery(sectionID);
    jQuery('body, html').animate({
        scrollTop: newSection.offset().top
    }, 'slow');
    
    newSection.addClass('active').siblings().removeClass('active');
    window.dispatchEvent(slideChange);
}

function createPathhNav(){
        jQuery('body').prepend('<div class="pathhNav container"><ul></ul></div>');
        slides.each(function(index){
            var screenLabel =  jQuery(this).find("span.screenLabel").text(),
                sectionID =  jQuery(this).attr('id');
            jQuery(this).attr('data-index', index); 
            jQuery('.pathhNav ul').append('<li class="index'+ index +'" data-sectionID="'+ sectionID +'" data-title="'+ screenLabel +'"><span class="puce"></span><p class="title">'+ screenLabel +'</p></li>')
        });

        jQuery('.pathhNav ul li').click(function(){
            scrollToSection('#' + jQuery(this).attr('data-sectionID'));
        });
        jQuery('.pathhNav li.index0').addClass('current');
}

function changeCurrentPathNav(){
    var currentSlide = jQuery('section.slides.active'),
        indexCurrentSlide = currentSlide.attr('data-index');

        jQuery('.pathhNav li').removeClass('current');
        jQuery('.pathhNav li.index' + indexCurrentSlide).addClass('current');
}