﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="emotion_01" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s002" data-screen-label="Barry is embarrassed and inconvenienced" data-screen-name="Barry is embarrassed and inconvenienced" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Have any of your older male patients developed LUTS-BPH* in addition to ED? Is this just a side effect of ageing, or might they be linked?</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">&lt;span&gt;Have any of your older male patients&lt;br/&gt; developed LUTS-BPH* in addition to ED?&lt;/span&gt;&lt;br/&gt;Is this just a side effect of ageing, or might they be linked?</h2>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#howmany_02" data-i18n="button">What's happening &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="legend onlyUK">*Lower Urinary Tract Symptoms associated with Benign Prostatic Hyperplasia</p>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
        <div class="orangeBubble">
            <span class="hidden" data-i18n="yellow_bubble">I want to be ready for intimacy whenever the moment is right - like before I had ED</span>
            <span class="hidden" data-i18n="yellow_bubble2">I want to know what to expect from my treatment</span>
            <span class="hidden" data-i18n="orange_bubble">It's frustrating to see my prescription list getting longer</span>
        </div>
         <div class="greyBubble">
            <span class="hidden" data-i18n="grey_bubble">I want to treat my urinary symptoms without feeling like an old man</span>
         </div>
    </div>
</section></body></html>
