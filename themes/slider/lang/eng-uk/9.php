﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="timeline_10" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s012" data-screen-label="Early improvements with Cialis 5 mg Once Daily" data-screen-name="Early improvements with Cialis 5 mg Once Daily" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Early improvements with Cialis 5&amp;nbsp;mg Once-Daily</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">&lt;span&gt;Early improvements&lt;/span&gt;&lt;br/&gt;with Cialis 5&amp;nbsp;mg Once-Daily</h2>
            </div>
            <div class="row">
              <div class="calendarContainer">
                <table class="calendar"><tr><th colspan="7" data-i18n="month">SEPTEMBER</th>
                  </tr><tr><td data-i18n="sunday">SUN</td>
                    <td data-i18n="monday">MON</td>
                    <td data-i18n="tuesday">TUE</td>
                    <td data-i18n="wednesday">WED</td>
                    <td data-i18n="thursday">THU</td>
                    <td data-i18n="friday">FRI</td>
                    <td data-i18n="saturday">SAT</td>
                  </tr><tr><td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>1</td>
                    <td class="greenSmBubble"><span>2</span></td>
                  </tr><tr><td>3</td>
                    <td class="greenSmBubble"><span>4</span></td>
                    <td>5</td>
                    <td>6</td>
                    <td>7</td>
                    <td>8</td>
                    <td>9</td>
                  </tr><tr><td>10</td>
                    <td>11</td>
                    <td>12</td>
                    <td>13</td>
                    <td class="yellowSmBubble"><span>14</span></td>
                    <td>15</td>
                    <td>16</td>
                  </tr><tr><td>17</td>
                    <td>18</td>
                    <td>19</td>
                    <td>20</td>
                    <td>21</td>
                    <td>22</td>
                    <td>23</td>
                  </tr><tr><td>24</td>
                    <td>25</td>
                    <td>26</td>
                    <td>27</td>
                    <td class="yellowSmBubble"><span>28</span></td>
                    <td></td>
                    <td></td>
                  </tr></table><div class="calendarBubbles leftTopBubble">
                    <p>
                      <span class="bubbleNumber">2</span><br><span class="bubbleFor" data-i18n="bubblefor1"></span><br><span data-i18n="green_bubble"></span>
                    </p>
                </div>
                <div class="calendarBubbles leftBottomBubble">
                    <p>
                      <span class="bubbleNumber">4</span><br><span class="bubbleFor" data-i18n="bubblefor2"></span><br><span data-i18n="green_bubble2">Can be effective in ED within four days of starting treatment&lt;sup&gt;10&lt;/sup&gt;</span>
                    </p>
                </div>
                <div class="calendarBubbles rightTopBubble">
                    <p>
                      <span class="bubbleNumber">14</span><br><span class="bubbleFor" data-i18n="bubblefor3"></span><br><span data-i18n="orange_bubble">Significantly improves IPSS vs baseline in as little as two weeks&lt;sup&gt;7&lt;/sup&gt;</span>
                    </p>
                </div>
                <div class="calendarBubbles rightBottomBubble">
                    <p>
                      <span class="bubbleNumber">28</span><br><span class="bubbleFor" data-i18n="bubblefor4"></span><br><span data-i18n="orange_bubble2">Significantly improves&lt;br/&gt; LUTS-BPH vs placebo at similar levels to tamsulosin in one month&lt;sup&gt;9&lt;/sup&gt;</span>
                    </p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 safetyInfo">
                <p data-i18n="safety_information"></p>
              </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#product_11" data-i18n="button">Summary &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
    </div>


    <!-- References Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel" data-i18n="title_popin_references">References</h4>
                </div>
                <div class="modal-body" data-i18n="popin_references">&lt;ol&gt;
    &lt;li&gt;&lt;span&gt;Braun MH, Sommer F, Haupt G et al. Lower Urinary Tract Symptoms and Erectile Dysfunction: CoMorbidity or Typical &lsquo;Aging Male&rsquo; Symptoms? Results of the &lsquo;Cologne Male Survey&rsquo;. Eur Urol. 2003;44(5):588-94.&lt;/span&gt;&lt;/li&gt;
    &lt;li&gt;&lt;span&gt;Rosen R, Altwein J, Boyle P et al. Lower Urinary Tract Symptoms and Male Sexual Dysfunction: The Multinational Survey of the Aging Male (MSAM-7). Eur Urol. 2003;44(6):637-49.&lt;/span&gt;&lt;/li&gt;
    &lt;li&gt;&lt;span&gt;Gravas S, Bach T, Bachmann A et al. Guidelines on the Management of Non-Neurogenic Male Lower Urinary Tract Symptoms (LUTS), incl Benign Prostatic Obstruction (BPO). Europ Assoc of Urology 2017.&lt;/span&gt;&lt;/li&gt;
    &lt;li&gt;&lt;span&gt;Rosen RC, Cappelleri JC, Gendrano N. The International Index of Erectile Function (IIEF): a state-of-the-science review. Int J Impot Res. 2002;14(4):226-44.&lt;/span&gt;&lt;/li&gt;
    &lt;li&gt;&lt;span&gt;Barry MJ, Fowler FJ, O&rsquo;Leary MP et al. The American Urological Association symptom index for benign prostatic hyperplasia. The Measurement Committee of the American Urological Association. J Urol. 1992;148(5):1549-57; discussion 1564.&lt;/span&gt;&lt;/li&gt;
    &lt;li&gt;&lt;span&gt;Cialis Summary of Product Characteristics, 2017.&lt;/span&gt;&lt;/li&gt;
    &lt;li&gt;&lt;span&gt;Egerdie RB, Auerbach S, Roehrborn CG et al. Tadalafil 2.5 or 5&amp;nbsp;mg administered once daily for 12 weeks in men with both erectile dysfunction and signs and symptoms of benign prostatichyperplasia: results of a randomized, placebo-controlled, double-blind study. J Sex Med. 2012;9(1):271-81.&lt;/span&gt;&lt;/li&gt;
    &lt;li&gt;&lt;span&gt;Rubio-Aurioles E, Kim ED, Rosen RC et al. Impact on erectile function and sexual quality of life of couples: a double-blind, randomized, placebo-controlled trial of tadalafil taken once daily. J Sex Med. 2009;6(5):1314-23.&lt;/span&gt;&lt;/li&gt;
    &lt;li&gt;&lt;span&gt;Oelke M, Giuliano F, Mirone V et al. Monotherapy with tadalafil or tamsulosin similarly improved lower urinary tract symptoms suggestive of benign prostatic hyperplasia in an international, randomised, parallel, placebo-controlled clinical trial. Eur Urol. 2012;61(5):917-25.&lt;/span&gt;&lt;/li&gt;
    &lt;li&gt;&lt;span&gt;Seftel A, Goldfischer E, Kim ED et al. Onset of efficacy of tadalafil once daily in men with erectile dysfunction: a randomized, double-blind, placebo controlled trial. J Urol. 2011;185(1):243-48.&lt;/span&gt;&lt;/li&gt;
&lt;/ol&gt;</div>
            </div>
        </div>
    </div>
    <!-- End References Modal -->

    <!-- PI Modal -->
    <div class="modal fade" id="piModal" tabindex="-1" role="dialog" aria-labelledby="piModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel" data-i18n="title_popin_pi">Prescribing Information</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="row">
                          <h3>CIALIS<sup>&reg;</sup> (tadalafil) Abbreviated Prescribing Information</h3>
                        </div>
                      <div class="row">
                          <div class="col-xs-12 col-sm-6 col-md-6">
                              <p>
                                <b>Presentation</b> Tablets 2.5mg, 5mg, 10mg, or 20mg of tadalafil. Also contains lactose. <b>Uses</b> Treatment of erectile dysfunction (ED) in adult males. <i>Cialis 5mg only:</i> Treatment of the signs and symptoms of benign prostatic hyperplasia (BPH) in adult males. <b>Dosage and Administration</b> <u>Posology:</u> <i>Erectile dysfunction</i> Adult men: The recommended dose is 10mg orally, taken at least 30 minutes prior to sexual activity. In those patients in whom tadalafil 10mg does not produce an adequate effect, 20mg might be tried. Maximum dosing frequency, once per day. 10mg or 20mg tadalafil is not recommended for continuous daily use. In patients who anticipate a frequent use of Cialis (ie, at least twice weekly), a once daily regimen with the lowest doses of Cialis might be considered. The recommended dose is 5mg taken once a day at approximately the same time of day. The dose may be decreased to 2.5mg once a day based on individual tolerability. The appropriateness of continued use of the daily regimen should be reassessed periodically. <i>Benign prostatic hyperplasia</i> 5mg taken at approximately the same time every day, with or without food. <u>Special populations:</u> <i>Elderly men:</i> Dosage adjustment not required. <i>Men with impaired renal or hepatic function:</i> In patients with severe renal impairment the maximum recommended dose is 10mg. Once a day dosing of Cialis is not recommended in patients with severe renal impairment. In men with hepatic impairment and ED the recommended dose is 10mg. There are no available data about the administration of doses higher than 10mg of tadalafil to patients with hepatic impairment. There is limited clinical data on the safety of Cialis in patients with severe hepatic impairment; if prescribed, a careful individual benefit/risk evaluation should be undertaken by the prescribing physician. Once a day dosing for both ED and BPH has not been evaluated in patients with hepatic impairment; therefore, if prescribed, a careful individual benefit/risk evaluation should be undertaken by the prescribing physician. <i>Men with diabetes:</i> Dosage adjustment not required. <i>Paediatric population:</i> There is no relevant use of Cialis in the paediatric population. In clinical trials, Cialis demonstrated improvement in patients&rsquo; erectile function and the ability to have successful sexual intercourse up to 36 hours following dosing. <b>Contra-indications</b> Known hypersensitivity to any ingredient. Patients using any form of organic nitrate. In men with cardiac disease for whom sexual activity is inadvisable. Physicians should consider the potential cardiac risk of sexual activity in patients with pre-existing cardiovascular disease. Patients with myocardial infarction within the last 90 days, unstable angina or angina occurring during sexual intercourse, New York Heart Association class 2 or greater heart failure in the last 6 months, uncontrolled arrhythmias, hypotension (&lt;90/50mmHg), or uncontrolled hypertension, a stroke within the last 6 months. Patients who have loss of vision in one eye because of non-arteritic anterior ischaemic optic neuropathy (NAION), regardless of whether this episode was in connection or not with previous PDE5 inhibitor exposure. Co-administration with guanylate cyclase stimulators, such as riociguat, as it may potentially lead to symptomatic hypotension. <b>Warnings and Special Precautions</b> <i>Before treatment with CIALIS:</i> A medical history and physical examination should be undertaken to diagnose ED or BPH and determine potential underlying causes, before pharmacological treatment is considered. Prior to any treatment for erectile dysfunction, physicians should consider the cardiovascular status of their patients, since there is a degree of cardiac risk associated with sexual activity. Tadalafil has vasodilator properties, resulting in mild and transient decreases in blood pressure. It augments the hypotensive effect of nitrates. Prior to initiating treatment with tadalafil for BPH, patients should be examined to rule out the presence of carcinoma of the prostate and carefully assessed for cardiovascular conditions. It is not known if Cialis is effective in patients who have undergone pelvic surgery or radical non-nerve-sparing prostatectomy. <i>Cardiovascular:</i> Serious cardiovascular events were reported either post-marketing and/or in clinical trials. Most of the patients in whom these events have been reported had pre-existing cardiovascular risk factors. However, it is not possible to definitively determine whether these events are related directly to these risk factors, to Cialis, to sexual activity, or to a combination of these or other factors. In patients receiving concomitant antihypertensive medicines, tadalafil may induce a blood pressure decrease. When initiating daily treatment with tadalafil, appropriate clinical considerations should be given to a possible dose adjustment of the antihypertensive therapy. <i>Vision:</i> Visual defects and cases of NAION have been reported in connection with the intake of Cialis and other PDE5 inhibitors. Analyses of observational data suggest an increased risk of acute NAION in men with erectile dysfunction following exposure to tadalafil or other PDE5 inhibitors. As this may be relevant for all patients exposed to tadalafil, the patient should be advised that in case of sudden visual defect, he should stop taking CIALIS and consult a physician immediately. <i>Decreased or sudden hearing loss:</i> Cases of sudden hearing loss have been reported after the use of tadalafil. Although other risk factors were present in some cases (such as age, diabetes, hypertension and previous hearing loss history) patients should be advised to stop taking tadalafil and seek prompt medical attention in the event of sudden decrease or loss of hearing. <i>Renal and hepatic impairment:</i> Due to increased tadalafil exposure (AUC), limited clinical experience, and the lack of ability to influence clearance by dialysis, once a day dosing of Cialis is not recommended in patients with severe renal impairment. There is limited clinical data on the safety of single-dose administration of tadalafil in patients with severe hepatic insufficiency (Child-Pugh class C);
                              </p>
                          </div>
                          <div class="col-xs-12 col-sm-6 col-md-6">
                              <p>
                                if prescribed, a careful individual benefit/risk evaluation should be undertaken by the prescribing physician. <i>Priapism and anatomical deformation of the penis:</i> Patients who experience erections lasting 4 hours or more should be instructed to seek medical assistance. If priapism is not treated immediately, penile tissue damage and permanent loss of potency may result. Use with caution in patients who have conditions that might predispose them to priapism, or in patients with anatomical deformation of the penis. <i>Lactose:</i> Cialis should not be administered to patients with hereditary problems of galactose intolerance, the Lapp lactase deficiency, or glucose-galactose malabsorption. <b>Interactions</b> Caution should be exercised when prescribing Cialis to patients using potent CYP3A4 inhibitors (ritonavir, saquinavir, ketoconazole, itraconazole, and erythromycin) as increased tadalafil exposure (AUC) has been observed if the drugs are combined. Inducers of CYP3A4, such as phenobarbital, phenytoin, and carbamazepine, may decrease plasma concentrations of tadalafil. In patients who are taking alpha1-blockers, concomitant administration of Cialis may lead to symptomatic hypotension in some patients. The combination of tadalafil and doxazosin is not recommended. The safety and efficacy of combinations of tadalafil and other PDE5 inhibitors or other treatments for ED have not been studied. Patients should be informed not to take Cialis with such combinations. Caution should be exercised when tadalafil is co-administered with 5-alpha reductase inhibitors, eg, finasteride. <b>Fertility, Pregnancy, and Lactation</b> Not indicated for use by women. <i>Fertility:</i> Effects were seen in dogs that might indicate impairment of fertility. Two subsequent clinical studies suggest that this effect is unlikely in humans, although a decrease in sperm concentration was seen in some men. <b>Effects on ability to drive and use machines</b> Cialis has negligible influence on the ability to drive or use machines. Although the frequency of reports of dizziness in placebo and tadalafil arms in clinical trials was similar, patients should be aware of how they react to Cialis before driving or operating machinery. <b>Undesirable Effects</b> The most commonly reported adverse reactions in patients taking Cialis for the treatment of ED or BPH were headache, dyspepsia, back pain, and myalgia, in which the incidences increase with increasing dose of Cialis. The adverse reactions reported were transient, and generally mild or moderate. The majority of headaches reported with Cialis once a day dosing are experienced within the first 10 to 30 days of starting treatment. <i>Common (&ge;1/100 to &lt;1/10):</i> Headache, flushing, dyspepsia, nasal congestion, back pain, myalgia, pain in extremity. <i>Uncommon (&ge;1/1,000 to &lt;1/100):</i> Hypersensitivity reactions, dizziness, blurred vision, sensations described as eye pain, tinnitus, tachycardia, palpitations, hypotension<sup>(3)</sup>, hypertension, abdominal pain, vomiting, nausea, gastro-oesophageal reflux, dyspnoea, epistaxis, rash, chest pain<sup>(1)</sup>, peripheral oedema, fatigue, haematuria, prolonged erections. <i>Rare (&ge;1/10,000 to &lt;1/1,000):</i> Angioedema<sup>(2)</sup>, stroke<sup>(1)</sup> (including haemorrhagic events), syncope, transient ischaemic attacks<sup>(1)</sup>, migraine<sup>(2)</sup>, seizures<sup>(2)</sup>, transient amnesia, visual field defect, swelling of eyelids, conjunctival hyperaemia, NAION<sup>(2)</sup>, retinal vascular occlusion<sup>(2)</sup>, sudden hearing loss, myocardial infarction, unstable angina pectoris<sup>(2)</sup>, ventricular arrhythmia<sup>(2)</sup>, urticaria, Stevens-Johnson syndrome<sup>(2)</sup>, exfoliative dermatitis<sup>(2)</sup>, hyperhidrosis (sweating), priapism, penile haemorrhage, haematospermia, facial oedema<sup>(2)</sup>, sudden cardiac death<sup>(1, 2)</sup>. <sup>(1)</sup>Most of the patients had pre-existing cardiovascular risk factors. <sup>(2)</sup>Post-marketing surveillance reported adverse reactions not observed in placebo-controlled clinical trials. <sup>(3)</sup>More commonly reported when tadalafil is given to patients who are already taking antihypertensive medicinal products. A slightly higher incidence of ECG abnormalities, primarily sinus bradycardia, has been reported in patients treated with tadalafil once a day as compared with placebo. Most of these ECG abnormalities were not associated with adverse reactions. Data in patients over 65 years of age receiving tadalafil in clinical trials, either for the treatment of ED or the treatment of BPH, are limited. In clinical trials with tadalafil taken on demand for the treatment of erectile dysfunction, diarrhoea was reported more frequently in patients over 65 years of age. In clinical trials with tadalafil 5mg taken once a day for the treatment of BPH, dizziness and diarrhoea were reported more frequently in patients over 75 years of age. <i>For full details of these and other side-effects, please see the Summary of Product Characteristics, which is available at <a target="_blank" href="http://www.medicines.org.uk/emc/">http://www.medicines.org.uk/emc/</a>.</i> <b>Legal Category</b> POM <b>Marketing Authorisation Numbers</b> EU/1/02/237/001 EU/1/02/237/002 EU/1/02/237/003 EU/1/02/237/004 EU/1/02/237/005 EU/1/02/237/006 EU/1/02/237/007 EU/1/02/237/008 <b>Cost</b> &pound;54.99 per pack of 28 X 2.5mg &pound;54.99 per pack of 28 X 5mg &pound;28.88 per pack of 4 X 10mg &pound;28.88 per pack of 4 X 20mg &pound;57.76 per pack of 8 X 20mg <b>Date of Preparation or Last Review</b> March 2017 <b>Full Prescribing Information is Available From</b> Eli Lilly and Company Limited Lilly House, Priestley Road Basingstoke, Hampshire, RG24 9NL Telephone: Basingstoke (01256) 315 000 E-mail: ukmedinfo@lilly.com CIALIS<sup>&reg;</sup> (tadalafil) a registered trademark of Eli Lilly and Company.
                              </p>
                              <p class="green">&nbsp;</p>
                              <p class="infoBox lightOrangeBckgrd">
                                  Adverse events should be reported.<br>
                                  Reporting forms and further information can be found at: <a href="http://www.mhra.gov.uk/yellowcard">www.mhra.gov.uk/yellowcard</a>.<br>
                                  Adverse events and product complaints should also be reported to Lilly: please call Lilly UK on 01256 315 000.
                              </p>
                          </div>
                      </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End PI Modal -->

    <!-- PI Modal -->
    <div class="modal fade" id="aeModal" tabindex="-1" role="dialog" aria-labelledby="aeModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel" data-i18n="title_popin_adverse">Report an Adverse Event</h4>
                </div>
                <div class="modal-body">
                    <p class="infoBox lightOrangeBckgrd">
                        Adverse events should be reported.<br>
                        Reporting forms and further information can be found at: <a href="http://www.mhra.gov.uk/yellowcard" target="_blank">www.mhra.gov.uk/yellowcard</a>.<br>
                        Adverse events and product complaints should also be reported to Lilly: please call Lilly UK on 01256 315 000.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- End PI Modal -->
    
</section></body></html>
