﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="diagnose_05" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s006" data-screen-label="Diagnosing ED and LUTS-BPH in patients like Barry" data-screen-name="Diagnosing ED and LUTS-BPH in patients like Barry" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Diagnosing ED and LUTS-BPH in your patients</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8">
                    <h2 data-i18n="screen_title">&lt;span&gt;Diagnosing &lt;br/&gt;ED and LUTS-BPH&lt;/span&gt;&lt;br/&gt;in your patients</h2>
                    <p class="subtitle" data-i18n="screen_subtitle">Since ED and LUTS-BPH are often related, you may need to consider assessing sexual and urinary symptoms - regardless of the patient's initial complaint.&lt;sup&gt;2,3&lt;/sup&gt;</p>
                    <p class="orange bold bigger" data-i18n="orange_title">Useful diagnostic tools are available to download here:</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-5 no-col-sm">
                    <div class="whiteBox">
                        <h5><a href="http://comms.univadis.com/s/lilly/cialis/201702_eDetailDownloads/edetail3/UK/UKCLS01862_Diagnostic-Tool-Pad.pdf?Expires=1504102249&amp;Signature=bGRT48uu7Wh-yLWu1CJYtQu~T2Fpw57HX3-V~trCJEswcRVYuUPMJW2obb49kr7GAaBINc31il8hcv-RiRg3gjwQktn8-94bwxhxtbowFVJ3OnrfVRqQ5hz4RtQLGfdG-xVTS6vCxjbL7DXEzuFgJWMnrWCy~vzBwv255cVtm9~MdWMkLmKzo~pFoYle3NaP7RDTrM-v94JRm9cc8B79AnQvSfdYzpN6vz0yrRUKYgpTRx44okw0VfkalXhC-muWsZzXcJevS5YAFmgTP5Tn7skYyk1zwkSYwU5inbTCWWHP2QDj~9n2rOTXjka5g87IaratthJJ~cGt84t45fn66Q__&amp;Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" class="gtmClickDownload" data-gtm="IIEF" data-i18n="left_list_title;[href]left_list_title_url" target="_blank">The Sexual Health Inventory for Men (SHIM or IIEF-5)&lt;sup&gt;4&lt;/sup&gt;</a></h5>
                        <ul class="list-group"><li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="left_list_item1">A simple 5-question assessment</span>
                            </li>
                            <li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="left_list_item2">May reduce the number of incorrectly&lt;br/&gt;diagnosed or under-diagnosed cases</span>
                            </li>
                            <li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="left_list_item3">Intended to complement a physical examination&lt;br/&gt;and patient history as a means to detect ED</span>
                            </li>
                        </ul></div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-5 no-col-sm">
                    <div class="whiteBox">
                        <h5><a href="http://comms.univadis.com/s/lilly/cialis/201702_eDetailDownloads/edetail3/UK/UKCLS01865_Diagnostic-Tool-Pad.pdf?Expires=1504102249&amp;Signature=i-jSweZ7XrXhIwd4D3gIf0YAR3OAclQKILnqM2tuK~A8Jv1~Yd1duRnPIlqKOCSe67e0DHLfdupoKTE7kGtPlXJ4vPwO4hKT1n5fKMY5nogHAWKlw3DP~oZE-ePp9dNuFULnEW7w07xJp6D7oH2Ms20I-LmKhXl-6JpvFEgqW8aCt0e9Vwb3HpbtWIX2BBhxnULGQ27I3vfVy0xTrdOTqw2MKfR21zoJ6H94WnJnhgMQ-0Xzf1ghXS9qflkvpcTOcSdQZPr8pKXjqMJCclSaAzrAvZImRHEkhkiXrH0G3zj3IhRYd2~10AiXVrX9x~786YqYUVnDlHlQYp8cjsm2lg__&amp;Key-Pair-Id=APKAJZZ3YHAGEKEXTA4A" class="gtmClickDownload" data-gtm="IPSS" data-i18n="right_list_title;[href]right_list_title_url" target="_blank">The International Prostate Symptom Score (IPSS)&lt;sup&gt;5&lt;/sup&gt;</a></h5>
                        <ul class="list-group"><li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="right_list_item1">A simple 8-item questionnaire</span>
                            </li>
                            <li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="right_list_item2">Aids in diagnosing LUTS-BPH and helps to assess treatment progress</span>
                            </li>
                        </ul></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="noUK" data-i18n="bottom_txt"></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#treatment_06" data-i18n="button">How to treat ED and LUTS-BPH &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
        <span class="hidden" data-i18n="grey_bubble"></span>
    </div>
</section></body></html>
