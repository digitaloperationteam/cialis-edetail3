﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="howmany_02" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s003" data-screen-label="How many men like Barry have these problems?" data-screen-name="How many men like Barry have these problems?" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">How many men have these problems?</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">How many men&lt;br/&gt;&lt;span&gt;have these problems?&lt;/span&gt;</h2>
                <div class="blc">
                    <h3 data-i18n="quizz_title">Move the cursor to the percentage of men that you think have both ED and LUTS-BPH</h3>
                   
                    <div id="slider"></div>
                    <div class="indicator onlyUK"><span></span></div>
                    <div id="dragItem">
                        <span id="men_green" class="men"></span>
                    </div>   
                    <button class="showAnswer gtmClickQuizShowAnswer" data-gtm="ShowQuizAnswer" type="submit" data-i18n="quizz_submit_button">Reveal answer</button>
                    <p id="quizz_answer" data-i18n="quizz_answer">&lt;span class='grey'&gt;72% of men &lt;i&gt;with&lt;/i&gt; ED have LUTS-BPH, but only 37.7% of men &lt;i&gt;without&lt;/i&gt; ED have LUTS-BPH.&lt;sup&gt;1&lt;/sup&gt;&lt;/span&gt;&lt;br/&gt;
LUTS-BPH co-exists with ED in many men, and is considered an age-independent risk factor for ED.&lt;sup&gt;2&lt;/sup&gt;</p>
                </div>
                <p class="legend" data-i18n="legend"></p>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#specialist_03" data-i18n="button">How to diagnose ED and LUTS-BPH &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
        </div>
    </div>
    <div class="paralaxElements">
        <span class="hidden" data-i18n="orange_bubble"></span>
    </div>
</section></body></html>
