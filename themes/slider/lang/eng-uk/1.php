﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="intro_00" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s001" data-screen-label="Home" data-screen-name="Home" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Home</span>

    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container"> 
            <div class="row">
                <div class="col-sm-12">
                    <p class="pitip onlyUK">Prescribing information can be found by scrolling to the bottom of the page</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#emotion_01" data-i18n="button">Read on &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
        </div>    
    </div> 
    <div class="paralaxElements">
        <h1 class="hidden" data-i18n="screen_title">Could you address two of your patient's problems with just one treatment?</h1>  
        <span class="hidden" data-i18n="menu_item1">Terms and Conditions</span>
        <span class="hidden" data-i18n="menu_item2">References</span>
        <span class="hidden" data-i18n="menu_item3">Prescribing Information</span>
        <span class="hidden" data-i18n="menu_item4">Privacy</span>
        <span class="hidden" data-i18n="menu_item5">Report an adverse event</span>
        <span class="hidden" data-i18n="menu_item6">Disclaimer</span>
        
    </div>
</section></body></html>
