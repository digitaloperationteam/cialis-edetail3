﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="graph_08" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s010" data-screen-label="Cialis benefits in ED and LUTS-BPH" data-screen-name="Cialis benefits in ED and LUTS-BPH" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Couples achieved sQoL domain scores as high as those experienced before ED</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">With Cialis 5&amp;nbsp;mg Once-Daily,&lt;br/&gt;&lt;span&gt;Couples achieved Sexual Quality of Life (sQoL) &lt;br/&gt;domain scores as high as those experienced &lt;br/&gt;before ED&lt;sup&gt;8&lt;/sup&gt;&lt;/span&gt;</h2>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-push-4">
                  <img class="img-responsive" src="images/content-graph-06.png" width="574" height="424"></div>
                <div class="v-center col-xs-12 col-md-4 col-md-pull-8">
                    <span class="round-icon check"></span>
                    <p class="text-center" data-i18n="graph_legend">Cialis 5&amp;nbsp;mg Once-Daily significantly improves sexual quality of life for both men with ED and their female partners.&lt;sup&gt;8&lt;/sup&gt;</p>
                    <div class="fishbone">
                          <p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#graph_09" data-i18n="button">And for LUTS-BPH... &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
      
    </div>
</section></body></html>
