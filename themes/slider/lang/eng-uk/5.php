﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="treatment_06" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s007" data-screen-label="One treatment for both Barry's problems" data-screen-name="One treatment for both Barry's problems" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Two problems, one treatment. Cialis 5&amp;nbsp;mg Once-Daily</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8">
                    <h2 data-i18n="screen_title">Two problems, one treatment.&lt;br/&gt;&lt;span&gt;Cialis 5&amp;nbsp;mg Once-Daily&lt;/span&gt;</h2>
                    <p data-i18n="screen_subtitle">Cialis 5&amp;nbsp;mg therapeutic indications&lt;sup&gt;6&lt;/sup&gt;
&lt;p&gt;Treatment of erectile dysfunction in adult males. In order for tadalafil to be effective for the treatment of erectile dysfunction, sexual stimulation is required.&lt;/p&gt;
&lt;p&gt;Treatment of the signs and symptoms of benign hyperplasia in adult males.&lt;/p&gt;</p>
                </div> 
            </div>
            <div class="row">
                <div id="quoteBlock" class="whiteBox col-sm-12 col-md-7">
                    <p class="text-center" data-i18n="quote_source">The 2017 European Association of Urology Guidelines on the management of LUTS-BPH state that:</p>
                    <blockquote data-i18n="quote">&rdquo;Although clinical trials of several selective oral PDE5 [inhibitors] have been conducted in men with LUTS, only tadalafil (5 mg once daily) has been licensed for the treatment of male LUTS.&rdquo;&lt;sup&gt;3&lt;/sup&gt;</blockquote>  
                    <span class="round-icon talk"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#how_07" data-i18n="button">What can patients expect from their treatment? &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
        <span class="hidden" data-i18n="yellow_bubble"></span>
    </div>
    
</section></body></html>
