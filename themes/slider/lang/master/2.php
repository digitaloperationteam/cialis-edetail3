﻿<section id="emotion_01" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s002" data-screen-label="Barry is embarrassed and inconvenienced" data-screen-name="Barry is embarrassed and inconvenienced" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">Barry is embarrassed and inconvenienced</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title"><span>Barry is embarrassed <br>and inconvenienced</span></h2>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#howmany_02" data-i18n="button">What's happening with Barry? <span class="caret"></span></button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="legend onlyUK">*Lower Urinary Tract Symptoms associated with Benign Prostatic Hyperplasia</p>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
        <div class="orangeBubble">
            <span class="hidden" data-i18n="yellow_bubble">I'm not getting much sleep - nor is my wife</span>
            <span class="hidden" data-i18n="yellow_bubble2">I don't know what to do, perhaps it's just my age.</span>
            <span class="hidden" data-i18n="orange_bubble">My whole working day is planned around toilet visits.</span>
        </div>
         <div class="greyBubble">
            <span class="hidden" data-i18n="grey_bubble">I get up several times each night to go to the toilet.</span>
         </div>
    </div>
</section>