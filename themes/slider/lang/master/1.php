﻿<section id="intro_00" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s001" data-screen-label="Home" data-screen-name="Home" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">Home</span>

    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container"> 
            <div class="row">
                <div class="col-sm-12">
                    <p class="pitip onlyUK">Prescribing information can be found by scrolling to the bottom of the page</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#emotion_01" data-i18n="button">Read on <span class="caret"></span></button>
                </div>
            </div>
        </div>    
    </div> 
    <div class="paralaxElements">
        <h1 class="hidden" data-i18n="screen_title">Solve two of Barry's problems with just one treatment</h1>  
        <span class="hidden" data-i18n="menu_item1">Terms and Conditions</span>
        <span class="hidden" data-i18n="menu_item2">References</span>
        <span class="hidden" data-i18n="menu_item3">Prescribing Information</span>
        <span class="hidden" data-i18n="menu_item4">Privacy</span>
        <span class="hidden" data-i18n="menu_item5">Cookies</span>
        <span class="hidden" data-i18n="menu_item6">Disclaimer</span>
        
    </div>
</section>