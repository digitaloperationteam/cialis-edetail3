﻿<section id="diagnose_05" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s006" data-screen-label="Diagnosing ED and LUTS-BPH in patients like Barry" data-screen-name="Diagnosing ED and LUTS-BPH in patients like Barry" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">Diagnosing ED and LUTS-BPH in patients like Barry</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8">
                    <h2 data-i18n="screen_title"><span>Diagnosing<br> ED and LUTS-BPH</span> <br>in patients like Barry
                    </h2>
                    <p class="subtitle" data-i18n="screen_subtitle">Since ED and LUTS-BPH are often related, you may need to consider assessing sexual and urinary afflictions - regardless of the patient's initial complaint.<sup>1,3,4</sup></p>
                    <p class="orange bold bigger" data-i18n="orange_title">Useful diagnostic tools:</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-5 no-col-sm">
                    <div class="whiteBox">
                        <h5><a href="#" class="gtmClickDownload" data-gtm="IIEF" data-i18n="left_list_title;[href]left_list_title_url" target="_blank">The Sexual Health Inventory for Men<br> (SHIM or IIEF-5)<sup>5</sup></a></h5>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="left_list_item1">A simple 5-question assessment</span>
                            </li>
                            <li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="left_list_item2">May reduce the number of incorrectly<br>diagnosed or under-diagnosed cases</span>
                            </li>
                            <li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="left_list_item3">Intended to complement a physical examination<br>and patient history as a means to detect ED</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-5 no-col-sm">
                    <div class="whiteBox">
                        <h5><a href="#" class="gtmClickDownload" data-gtm="IPSS" data-i18n="right_list_title;[href]right_list_title_url" target="_blank">The International Prostate Symptom<br> Score (IPSS)<sup>6</sup></a></h5>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="right_list_item1">A simple 8-item questionnaire</span>
                            </li>
                            <li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="right_list_item2">Aids in diagnosing LUTS-BPH and helps to assess treatment progress</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="noUK" data-i18n="bottom_txt">You can download an ED/LUTS-BPH diagnosis toolkit at the end of this presentation.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#treatment_06" data-i18n="button">How to treat ED and LUTS-BPH <span class="caret"></span></button>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
        <span class="hidden" data-i18n="grey_bubble">Is there something my doctor could do to help? Maybe he'll know what my problem is.</span>
    </div>
</section>