﻿<section id="specialist_04" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s005" data-screen-label="Barry stands to benefit from diagnosis and treatment of both his conditions" data-screen-name="Barry stands to benefit from diagnosis and treatment of both his conditions" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">Barry stands to benefit from diagnosis and treatment of both his conditions</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">Barry stands to benefit from<br><span>diagnosis and treatment</span><br>of both his conditions</h2>
            </div>
            <div class="row">
                <div class="infos-products">
                    <div class="graphContainer col-sm-12 col-md-5">
                        <div class="graph lightgreen pull-right">
                            <iframe class="chartjs-hidden-iframe" tabindex="-1" style="width: 100%; display: block; border: 0px none; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;">
                            </iframe>
                            <div class="whiteCircle"></div>
                            <canvas id="lightgreenChart" width="210" height="210" style="display: block; width: 210px; height: 210px;"></canvas>
                        </div>
                        <div class="graph mediumgreen pull-right">
                            <iframe class="chartjs-hidden-iframe" tabindex="-1" style="width: 100%; display: block; border: 0px none; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;">
                            </iframe>
                            <div class="whiteCircle"></div>
                            <canvas id="mediumgreenChart" width="210" height="210" style="display: block; width: 210px; height: 210px;"></canvas>
                        </div>
                        <div class="graph darkgreen pull-right">
                            <iframe class="chartjs-hidden-iframe" tabindex="-1" style="width: 100%; display: block; border: 0px none; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;">
                            </iframe>
                            <div class="whiteCircle"></div>
                            <canvas id="darkgreenChart" width="210" height="210" style="display: block; width: 210px; height: 210px;"></canvas>
                        </div>
                    </div>
                    <div class="infoGraph col-sm-12 col-md-5">
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <span class="dark-green text-right">19%</span> 
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9">
                                <p data-i18n="darkgreen_legend">Urologists estimated that 19% of their patients experienced sexual dysfunction because of LUTS<sup>14</sup>.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <span class="medium-green text-right">27%</span> 
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9">
                                <p data-i18n="mediumgreen_legend">while PCPs* estimated that 27% of their patients experienced sexual dysfunction because of LUTS<sup>14</sup>.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <span class="light-green text-right">62%</span> 
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9">
                                <p data-i18n="lightgreen_legend">These estimates contrast sharply with data from epidemiological studies, which suggest prevalence rates of ED and EjD in excess of 50% in men with BPH/LUTS<sup>15</sup>.</p>
                            </div>
                        </div>
                        <div class="row">
                            <p data-i18n="paragraph_quote"><strong><i>"It is likely that a substantial proportion of men who present with BPH/ LUTS will already have some degree of sexual dysfunction. It is therefore important to thoroughly assess sexual function before initiating pharmacotherapy for BPH/ LUTS"<sup>14</sup></i></strong></p>
                            <p class="legend" data-i18n="legend">*Primary care physicians</p> 
                        </div>
                                     
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#diagnose_05" data-i18n="button">How to diagnose ED and LUTS-BPH <span class="caret"></span></button>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
    </div>
</section>