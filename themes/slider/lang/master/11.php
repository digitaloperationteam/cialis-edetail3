﻿<section id="how_07" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s009" data-screen-label="
How Cialis works in LUTS-BPH" data-screen-name="How Cialis works in LUTS-BPH" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">How Cialis works in LUTS-BPH</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
              <div class="col-xs-12 col-md-8">
                <h2 data-i18n="screen_title"><span>How Cialis works</span> <br>in LUTS-BPH</h2>
                <p class="listTitle" data-i18n="list_title">Cialis 5 mg Once Daily has been shown to be effective in LUTS-BPH:<sup>3</p>
                <ul class="checkedList">
                  <li>
                      <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                      <span data-i18n="list_item1">22-37% reduction in the International Prostate Symptom Score (IPSS)</span>
                  </li>
                  <li>
                      <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                      <span data-i18n="list_item2">Significant improvements in urinary flow rate (Qmax)</span>
                  </li>
                  <li>
                      <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                      <span data-i18n="list_item3">Improvements in patient quality of life (QoL)</span>
                  </li>
                </ul>
                <div class="row">
                  <div class="playListVideo col-sm-12">
                      <div class="video whiteBox">
                        <a class="BC-openPopin" data-target="bcVideo1" data-videodescription="" data-videoid="5533993923001" data-videoposition="" href="#" title="">
                          <div class="row">
                            <div class="videoImage">
                              <img class="BC-thumbnail" border="0" src="images/master/img_video.jpg" width="240" height="140" />
                            </div>
                            <div class="text">
                              <h3 class="BC-title" data-i18n="title_video1">See how Cialis can target LUTS-BPH.</h3>
                              <p class="BC-description" data-i18n="description_video1">> watch this short video</p>
                            </div>
                          </div>
                        </a>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-md-8 safetyInfo">
                <p data-i18n="safety_information">SAFETY INFORMATION: Cialis is generally well-tolerated. The most common side effects seen in clinical trials including 8022 men were headache, back pain, muscle aches, arm or leg pain, facial flushing, nasal congestion and indigestion.<sup>7</sup></p>
              </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#graph_08" data-i18n="button">What can Barry expect from his treatment? <span class="caret"></span></button>
                </div>
            </div>
        </div> 
    </div>

    <div id="overlayVideo" class="BC-playlistOverlay">
        <span class="popin-close-icon BC-close-popin"></span>
        <div style="display: block; position: relative; max-width: 100%;">
            <div id="BC-contentOverlay">
                <video id="BC-playerHTML5" data-video-id="5533993923001" 
                data-account="4931690864001" 
                data-player="rJSbLDiJe" 
                data-embed="default" 
                data-application-id 
                class="video-js" 
                controls 
                style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;">
                </video>
            </div>
        </div>
    </div>

    <script src="//players.brightcove.net/4931690864001/rJSbLDiJe_default/index.min.js"></script>
    <div class="paralaxElements">
      <span class="hidden" data-i18n="green_bubble">It's a relief to be able to go regularly again, and not have to always be looking for the nearest bathroom.</span>
    </div>
</section>