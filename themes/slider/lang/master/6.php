﻿<section id="specialist_03" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s004" data-screen-label="Barry stands to benefit from diagnosis and treatment of both his conditions" data-screen-name="Barry stands to benefit from diagnosis and treatment of both his conditions" data-screen-section="">
    <span class="hidden screenLabel" data-i18n="screenLabel">Barry stands to benefit from diagnosis and treatment of both his conditions</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">Barry stands to benefit from<br><span>diagnosis and treatment</span><br>of both his conditions</h2>
            </div>
            <div class="row">
                <div class="col-xs-4 col-md-4">
                    <div class="doc pull-right">
                        <div data-i18n="doctor_name" class="circle">Dr Jon Rees</div>
                        <p class="text-center" data-i18n="doctor_profession">GP specialising in Urology and Men's Health</p>
                    </div>
                </div>
                <div class="bullesSms col-xs-8 col-md-6">
                    <p data-i18n="doctor_text1" class="bulle">There's strong evidence to show these two conditions are strongly linked.</p>
                    <p data-i18n="doctor_text2" class="bulle">If you can identify both those conditions in these men, you do have the opportunity to delve into wider issues around men's health.</p>
                    <p data-i18n="doctor_text3" class="bulle">There is a significant cohort of men out there who are comorbid with both conditions, have significant bother with both conditions, and where treatment with one drug that treats both, may be a very appropriate form of treatment.</p>
                </div>
            </div>    
            <div class="row">
                <button type="button" class="btn btn-default scrollToSection center-block" href="#specialist_04" data-i18n="button">How to diagnose ED and LUTS-BPH <span class="caret"></span></button>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
    </div>
</section>