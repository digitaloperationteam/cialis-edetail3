﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="product_11" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s014" data-screen-label="Help patients like Barry regain their previous lifestyle" data-screen-name="Help patients like Barry regain their previous lifestyle" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Aiuti i suoi pazienti a ritrovare lo stile di vita di una volta...</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">Aiuti i pazienti con DE e IPB-LUTS&lt;br/&gt;&lt;span&gt;a ritrovare lo stile di vita di una volta...&lt;/span&gt;</h2>
                <h3 data-i18n="screen_subtitle">Cialis 5 mg Terapia Giornaliera&lt;br/&gt;Due problemi, un solo trattamento per la DE e gli IPB-LUTS:&lt;sub&gt;7&lt;/sub&gt;</h3>
            </div>
            <div class="row">
              <div class="infos-products">
                <div class="col-xs-12 col-sm-6">
                    <img src="img/product-5mg.png" alt="" class="img-responsive"></div>
                <div class="v-center col-xs-12 col-sm-6">
                    <ul><li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <span data-i18n="list_item1">Per aiutare i pazienti con DE a recuperare la spontaneit&agrave; sessuale con le loro partner&lt;sup&gt;8,12&lt;/sup&gt;</span><br><span data-i18n="list_item1_content">Affinch&eacute; Cialis possa essere efficace nel trattamento della disfunzione erettile &egrave; necessaria la stimolazione sessuale.&lt;sup&gt;7&lt;/sup&gt;</span></li>
                      <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <span data-i18n="list_item2">Per alleviare il disagio urinario associato agli IPB-LUTS&lt;sup&gt;13,16&lt;/sup&gt;</span><br><span data-i18n="list_item2_content">Indicato per il trattamento dei segni e dei sintomi dell&rsquo;iperplasia prostatica benigna negli uomini adulti.&lt;sup&gt;7&lt;/sup&gt;</span></li>
                    </ul></div>
              </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
    </div>
    
</section></body></html>
