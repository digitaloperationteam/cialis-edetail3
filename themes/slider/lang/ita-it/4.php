﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="specialist_04" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s005" data-screen-label="Barry stands to benefit from diagnosis and treatment of both his conditions" data-screen-name="Barry stands to benefit from diagnosis and treatment of both his conditions" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Il suo paziente potr&agrave; trarre benefici dalla diagnosi e dal trattamento di entrambi i suoi disturbi</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">Il suo paziente potr&agrave; trarre benefici dalla&lt;br/&gt;&lt;span&gt;diagnosi e dal trattamento&lt;/span&gt;&lt;br/&gt;di entrambi i suoi disturbi</h2>
            </div>
            <div class="row">
                <div class="infos-products">
                    <div class="graphContainer col-sm-12 col-md-5">
                        <div class="graph lightgreen pull-right">
                            <iframe class="chartjs-hidden-iframe" tabindex="-1" style="width: 100%; display: block; border: 0px none; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;">
                            </iframe>
                            <div class="whiteCircle"></div>
                            <canvas id="lightgreenChart" width="210" height="210" style="display: block; width: 210px; height: 210px;"></canvas></div>
                        <div class="graph mediumgreen pull-right">
                            <iframe class="chartjs-hidden-iframe" tabindex="-1" style="width: 100%; display: block; border: 0px none; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;">
                            </iframe>
                            <div class="whiteCircle"></div>
                            <canvas id="mediumgreenChart" width="210" height="210" style="display: block; width: 210px; height: 210px;"></canvas></div>
                        <div class="graph darkgreen pull-right">
                            <iframe class="chartjs-hidden-iframe" tabindex="-1" style="width: 100%; display: block; border: 0px none; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;">
                            </iframe>
                            <div class="whiteCircle"></div>
                            <canvas id="darkgreenChart" width="210" height="210" style="display: block; width: 210px; height: 210px;"></canvas></div>
                    </div>
                    <div class="infoGraph col-sm-12 col-md-5">
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <span class="dark-green text-right">19%</span> 
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9">
                                <p data-i18n="darkgreen_legend">Gli urologi stimano che il 19% dei loro pazienti ha manifestato una disfunzione sessuale a causa dei LUTS&lt;sup&gt;14&lt;/sup&gt;,</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <span class="medium-green text-right">27%</span> 
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9">
                                <p data-i18n="mediumgreen_legend">mentre i Medici di Medicina Generale stimano che la percentuale dei loro pazienti che hanno manifestato una disfunzione sessuale a causa dei LUTS sia del 27%&lt;sup&gt;14&lt;/sup&gt;.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <span class="light-green text-right">62%</span> 
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-9">
                                <p data-i18n="lightgreen_legend">Queste stime sono in netto contrasto con i dati provenienti da studi epidemiologici, che suggeriscono tassi di prevalenza di DE e di disfunzione eiaculatoria (EjD) superiori del 50% negli uomini con IPB/LUTS&lt;sup&gt;15&lt;/sup&gt;.</p>
                            </div>
                        </div>
                        <div class="row">
                            <p data-i18n="paragraph_quote">&lt;strong&gt;&lt;i&gt;&ldquo;&Egrave; probabile che una percentuale consistente di uomini con IPB/LUTS presenti gi&agrave; un certo grado di disfunzione sessuale. &Egrave; quindi importante valutare accuratamente la funzione sessuale prima di avviare la farmacoterapia per gli IPB/LUTS&rdquo;&lt;sup&gt;14&lt;/sup&gt;&lt;/i&gt;&lt;/strong&gt;</p>
                            <p class="legend" data-i18n="legend"></p> 
                        </div>
                                     
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#diagnose_05" data-i18n="button">Come diagnosticare la DE e gli IPB-LUTS &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
    </div>
</section></body></html>
