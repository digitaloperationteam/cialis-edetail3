﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="graph_09" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s011" data-screen-label="A conversation tool kit to help you help Barry" data-screen-name="A conversation tool kit to help you help Barry" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Cialis offre benefici sia in caso di DE che di IPB-LUTS 2/2</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row onlyUK">
                <h2 data-i18n="screen_title">Cialis 5 mg Terapia Giornaliera&lt;br/&gt;&lt;span&gt;Benefici per gli IPB-LUTS&lt;/span&gt;</h2>
            </div>
            <div class="row">
                <div class="v-center col-md-4">
                    <div class="fishbone">
                          <p>&nbsp;</p>
                    </div>
                    <span class="round-icon check"></span>
                    <p class="text-center" data-i18n="graph_legend">Cialis 5 mg Terapia Giornaliera pu&ograve; fornire un miglioramento significativo degli IPB-LUTS, come misurato dall&rsquo;International Prostate Symptom Score (IPSS), con un'efficacia simile a tamsulosina 0,4 mg&lt;sup&gt;10&lt;/sup&gt;</p>
                </div>
                <div class="col-md-8">
                  <img class="img-responsive" src="images/content-graph-07.png" width="574" height="489"></div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#timeline_10" data-i18n="button">Dopo quanto tempo agisce? &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>

        </div> 
    </div>
    <div class="paralaxElements">
    </div>
</section></body></html>
