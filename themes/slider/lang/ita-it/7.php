﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="how_07" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s009" data-screen-label="
How Cialis works in LUTS-BPH" data-screen-name="How Cialis works in LUTS-BPH" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Come agisce Cialis sugli IPB-LUTS</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
              <div class="col-xs-12 col-md-8">
                <h2 data-i18n="screen_title">&lt;span&gt;Come agisce Cialis&lt;/span&gt; &lt;br/&gt;sugli IPB-LUTS</h2>
                <p class="listTitle" data-i18n="list_title">Cialis 5 mg Terapia Giornaliera si &egrave; dimostrato&lt;br/&gt;efficace nel trattamento degli IPB-LUTS:&lt;sup&gt;3&lt;/sup&gt;</p>
                <ul class="checkedList"><li>
                      <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                      <span data-i18n="list_item1">Riduzione del 22-37% nell&rsquo;International Prostate Symptom Score (IPSS)</span>
                  </li>
                  <li>
                      <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                      <span data-i18n="list_item2">Miglioramenti significativi della velocit&agrave; del flusso urinario (Qmax)</span>
                  </li>
                  <li>
                      <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                      <span data-i18n="list_item3">Miglioramenti della qualit&agrave; di vita del paziente (QoL)</span>
                  </li>
                </ul><div class="row">
                  <div class="playListVideo col-sm-12">
                      <div class="video whiteBox">
                        <a class="BC-openPopin" data-target="bcVideo1" data-videodescription="" data-videoid="5533993923001" data-videoposition="" href="#" title="">
                          <div class="row">
                            <div class="videoImage">
                              <img class="BC-thumbnail" border="0" src="images/master/img_video.jpg" width="240" height="140"></div>
                            <div class="text">
                              <h3 class="BC-title" data-i18n="title_video1">Scopra come Cialis pu&ograve; agire contro gli IPB-LUTS.</h3>
                              <p class="BC-description" data-i18n="description_video1">&gt; guardi questo breve video</p>
                            </div>
                          </div>
                        </a>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-md-8 safetyInfo">
                <p data-i18n="safety_information">INFORMAZIONI SULLA SICUREZZA: Cialis &egrave; generalmente ben tollerato. Gli effetti collaterali osservati pi&ugrave; spesso nelle sperimentazioni cliniche, effettuate su 8022 uomini, sono stati cefalea, dolore alla schiena, dolori muscolari, dolori a braccia o gambe, arrossamento del viso, congestione nasale e indigestione&lt;sup&gt;7&lt;/sup&gt;.</p>
              </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#graph_08" data-i18n="button">Cosa pu&ograve; aspettarsi il suo paziente dal trattamento? &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
        </div> 
    </div>

    <div id="overlayVideo" class="BC-playlistOverlay">
        <span class="popin-close-icon BC-close-popin"></span>
        <div style="display: block; position: relative; max-width: 100%;">
            <div id="BC-contentOverlay">
                <video id="BC-playerHTML5" data-video-id="5533993923001" data-account="4931690864001" data-player="rJSbLDiJe" data-embed="default" data-application-id class="video-js" controls style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;"></video></div>
        </div>
    </div>

    <script src="//players.brightcove.net/4931690864001/rJSbLDiJe_default/index.min.js"></script><div class="paralaxElements">
      <span class="hidden" data-i18n="green_bubble">&Egrave; un sollievo per me poter di nuovo andare in bagno regolarmente, senza essere sempre alla ricerca di quello pi&ugrave; vicino.</span>
    </div>
</section></body></html>
