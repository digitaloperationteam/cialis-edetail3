﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="diagnose_05" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s006" data-screen-label="Diagnosing ED and LUTS-BPH in patients like Barry" data-screen-name="Diagnosing ED and LUTS-BPH in patients like Barry" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Diagnosticare la DE e gli IPB-LUTS nei suoi pazienti</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8">
                    <h2 data-i18n="screen_title">&lt;span&gt;Diagnosticare la DE&lt;br/&gt; e gli IPB-LUTS&lt;/span&gt;&lt;br/&gt;nei suoi pazienti</h2>
                    <p class="subtitle" data-i18n="screen_subtitle">Dal momento che la DE e e gli IPB-LUTS sono spesso correlati, potrebbe essere&lt;br/&gt; necessario valutare sia i problemi sessuali che quelli urinari, indipendentemente&lt;br/&gt; da quanto riportato dal paziente all&rsquo;inizio.&lt;sup&gt;1,3,4&lt;/sup&gt;</p>
                    <p class="orange bold bigger" data-i18n="orange_title">Strumenti diagnostici utili:</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-5 no-col-sm">
                    <div class="whiteBox">
                        <h5><a href="http://comms.univadis.com/p/lilly/cialis/201702_eDetail3/eDetail/IIEF-5.pdf" class="gtmClickDownload" data-gtm="IIEF" data-i18n="left_list_title;[href]left_list_title_url" target="_blank">The Sexual Health Inventory for Men&lt;br/&gt; (SHIM o IIEF-5)&lt;sup&gt;5&lt;/sup&gt;</a></h5>
                        <ul class="list-group"><li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="left_list_item1">Una semplice valutazione composta da 5 domande</span>
                            </li>
                            <li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="left_list_item2">Pu&ograve; ridurre il numero di&lt;br/&gt;diagnosi errate o di casi sotto-diagnosticati</span>
                            </li>
                            <li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="left_list_item3">Progettata per integrare l&rsquo;esame obiettivo&lt;br/&gt;e l&rsquo;anamnesi del paziente come strumenti per rilevare la DE</span>
                            </li>
                        </ul></div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-5 no-col-sm">
                    <div class="whiteBox">
                        <h5><a href="http://comms.univadis.com/p/lilly/cialis/201702_eDetail3/eDetail/IPSS.pdf" class="gtmClickDownload" data-gtm="IPSS" data-i18n="right_list_title;[href]right_list_title_url" target="_blank">The International Prostate Symptom&lt;br/&gt; Score (IPSS)&lt;sup&gt;6&lt;/sup&gt;</a></h5>
                        <ul class="list-group"><li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="right_list_item1">Un semplice questionario composto da 8 domande</span>
                            </li>
                            <li class="list-group-item">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span data-i18n="right_list_item2">Aiuta a diagnosticare gli IPB-LUTS e a valutare i progressi del trattamento</span>
                            </li>
                        </ul></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="noUK" data-i18n="bottom_txt">In fondo a questa presentazione &egrave; possibile scaricare un kit di strumenti diagnostici per DE/IPB-LUTS.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#treatment_06" data-i18n="button">Come trattare la DE e gli IPB-LUTS &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
        <span class="hidden" data-i18n="grey_bubble">Cosa pu&ograve; fare il mio medico per aiutarmi? Forse lui sa qual &egrave; il mio problema.</span>
    </div>
</section></body></html>
