﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="download_13" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s015" data-screen-label="A diagnosis tool kit to help you help Barry" data-screen-name="A diagnosis tool kit to help you help Barry" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Un kit di strumenti diagnostici per aiutarla ad aiutare il suo paziente</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">Un kit di strumenti diagnostici &lt;br/&gt;&lt;span&gt;per aiutarla ad aiutare il suo paziente&lt;/span&gt;</h2>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-5 no-col-sm">
                  <div class="blc">
                     <h3 data-i18n="downloads_title">Download</h3>
                     <ul id="donwload-links" class="list-group"></ul></div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-5 no-col-sm">
                  <div class="blc">
                     <h3 data-i18n="videos_titl">Video</h3>
                     <div id="playlist-videos">
                     </div>
                  </div>
              </div>
              
              
            </div>
            <div id="footer" class="row itOnly">
              <div class="col-sm-12">
                <p class="mentions text-center">
                  ITCLS00991 data di deposito AIFA 03/08/2017
                </p>
              </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
    </div>

</section></body></html>
