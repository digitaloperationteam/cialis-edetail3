﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="howmany_02" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s003" data-screen-label="How many men like Barry have these problems?" data-screen-name="How many men like Barry have these problems?" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Quanti uomini come il suo paziente soffrono di questi problemi?</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">Quanti uomini come il suo paziente&lt;br/&gt;&lt;span&gt;soffrono di questi problemi?&lt;/span&gt;</h2>
                <div class="blc">
                    <h3 data-i18n="quizz_title">Quale percentuale di uomini con disfunzione erettile (DE) soffre anche di IPB-LUTS?*</h3>
                   
                    <div id="slider"></div>
                    <div class="indicator onlyUK"><span></span></div>
                    <div id="dragItem">
                        <span id="men_green" class="men"></span>
                    </div>   
                    <button class="showAnswer gtmClickQuizShowAnswer" data-gtm="ShowQuizAnswer" type="submit" data-i18n="quizz_submit_button">Inoltra</button>
                    <p id="quizz_answer" data-i18n="quizz_answer">L&rsquo;incidenza e la gravit&agrave; dei LUTS e della DE aumentano con l&rsquo;et&agrave;, soprattutto negli uomini oltre i 50 anni.&lt;sup&gt;2&lt;/sup&gt;</p>
                </div>
                <p class="legend" data-i18n="legend">*Sintomi del basso tratto urinario correlati a iperplasia prostatica benigna</p>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#specialist_03" data-i18n="button">Cosa dicono gli esperti? &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
        </div>
    </div>
    <div class="paralaxElements">
        <span class="hidden" data-i18n="orange_bubble">Problemi di erezione e problemi ad andare in bagno: tutto ci&ograve; &egrave; dovuto solo all&rsquo;invecchiamento?</span>
    </div>
</section></body></html>
