﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="emotion_01" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s002" data-screen-label="Barry is embarrassed and inconvenienced" data-screen-name="Barry is embarrassed and inconvenienced" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Il suo paziente &egrave; imbarazzato e infastidito</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <h2 data-i18n="screen_title">&lt;span&gt;Il suo paziente &egrave;&lt;br/&gt; imbarazzato e infastidito&lt;/span&gt;</h2>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#howmany_02" data-i18n="button">Cosa sta succedendo al suo paziente? &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p class="legend onlyUK">*Lower Urinary Tract Symptoms associated with Benign Prostatic Hyperplasia</p>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
        <div class="orangeBubble">
            <span class="hidden" data-i18n="yellow_bubble">Non dormo abbastanza e neanche mia moglie riesce a riposare bene</span>
            <span class="hidden" data-i18n="yellow_bubble2">Non so cosa fare, forse &egrave; solo una questione di et&agrave;.</span>
            <span class="hidden" data-i18n="orange_bubble">La mia intera giornata lavorativa &egrave; pianificata in base alle volte in cui devo andare in bagno.</span>
        </div>
         <div class="greyBubble">
            <span class="hidden" data-i18n="grey_bubble">Ogni notte mi alzo varie volte per andare in bagno.</span>
         </div>
    </div>
</section></body></html>
