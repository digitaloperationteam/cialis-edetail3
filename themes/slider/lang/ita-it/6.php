﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="treatment_06" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s007" data-screen-label="One treatment for both Barry's problems" data-screen-name="One treatment for both Barry's problems" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Un solo trattamento per entrambi i problemi del suo paziente</span>
    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8">
                    <h2 data-i18n="screen_title">&lt;span&gt;Un solo trattamento&lt;br/&gt;per entrambi&lt;/span&gt;&lt;br/&gt;i problemi del suo paziente</h2>
                    <p data-i18n="screen_subtitle">Cialis 5 mg Terapia Giornaliera &egrave; indicato sia per la DE che per gli IPB-LUTS.&lt;sup&gt;7&lt;/sup&gt;</p>
                </div> 
            </div>
            <div class="row">
                <div id="quoteBlock" class="whiteBox col-sm-12 col-md-7">
                    <p class="text-center" data-i18n="quote_source">Le linee guida 2016 dell&rsquo;Associazione Europea di Urologia (EAU) sulla gestione degli IPB-LUTS dichiarano:&lt;sup&gt;3&lt;/sup&gt;</p>
                    <blockquote data-i18n="quote">"Sebbene siano stati condotti studi clinici con una serie di PDE5 orali selettivi (inibitori) su uomini con LUTS, solo (Cialis) tadalafil (5mg Terapia Giornaliera) &egrave; stato autorizzato per il trattamento dei LUTS maschili"</blockquote>  
                    <span class="round-icon talk"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#how_07" data-i18n="button">Come agisce Cialis sugli IPB-LUTS? &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
        </div> 
    </div>
    <div class="paralaxElements">
        <span class="hidden" data-i18n="yellow_bubble">Non voglio dover assumere un&rsquo;altra pillola</span>
    </div>
    
</section></body></html>
