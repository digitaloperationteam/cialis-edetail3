﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><section id="intro_00" class="slides row" data-locale="eng-uk" data-screen-id="2017_CIA_ed3_s001" data-screen-label="Home" data-screen-name="Home" data-screen-section=""><span class="hidden screenLabel" data-i18n="screenLabel">Home</span>

    <div class="col-xs-12 col-sm-12 col-md-12 content">
        <div class="container"> 
            <div class="row">
                <div class="col-sm-12">
                    <p class="pitip onlyUK">Prescribing information can be found by scrolling to the bottom of the page</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-default scrollToSection center-block" href="#emotion_01" data-i18n="button">Continua a leggere &lt;span class="caret"&gt;&lt;/span&gt;</button>
                </div>
            </div>
        </div>    
    </div> 
    <div class="paralaxElements">
        <h1 class="hidden" data-i18n="screen_title">Risolvere due problemi del paziente con un solo trattamento</h1>  
        <span class="hidden" data-i18n="menu_item1">Termini e Condizioni</span>
        <span class="hidden" data-i18n="menu_item2">Bibliografia</span>
        <span class="hidden" data-i18n="menu_item3">Informazioni prescrittive</span>
        <span class="hidden" data-i18n="menu_item4">Privacy</span>
        <span class="hidden" data-i18n="menu_item5">Cookies</span>
        <span class="hidden" data-i18n="menu_item6">Dichiarazione di non responsabilit&agrave;</span>
        
    </div>
</section></body></html>
