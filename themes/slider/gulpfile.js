//Depedencies
var gulp        = require('gulp'),
    merge       = require('merge-stream'), //Using multiple sources at the same time
    runSequence = require('run-sequence'), //Get another sequence than 'default' to run. Can have a callback function
    useref      = require('gulp-useref'), //To concatenate css files or js scripts into one file (not minifying it)
    cssnano     = require('gulp-cssnano'), //To minify css
    sass        = require('gulp-sass'), //Create css from sass file
    imagemin    = require('gulp-imagemin'), //Minify images
    cache       = require('gulp-cache'), //Delete cache
    uglify      = require('gulp-uglify'), //To minify js scripts
    gulpIf      = require('gulp-if'), //To make a condition inside the pipe
    del         = require('del'), //Delete files
    order       = require("gulp-order"),
    webserver   = require('gulp-webserver'),
    opn         = require('opn'),
    connect     = require('gulp-connect-php'),
    browserSync = require('browser-sync');
 
// Distribution folder
var dest = "dist/";

gulp.task('inc', function() {
  return gulp.src('app/inc/**/*.php')
  .pipe(gulp.dest(dest + 'inc'))
})

gulp.task('fonts', function() {
  return gulp.src('app/fonts/*.{otf,eot,svg,ttf,woff,woff2}')
  .pipe(gulp.dest(dest + 'fonts'))
})

gulp.task('audio', function() {
  return gulp.src('app/audio/*.{wav,mp3}')
  .pipe(gulp.dest(dest + 'audio'))
})

//Concatenate JS plugin
gulp.task('useref', function(){
  return gulp.src('app/*.php')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    // Concatenate only if it's a CSS file
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest(dest))
});

// Converts Sass to CSS with gulp-sass
gulp.task('sass', function(){
  var main = gulp.src('app/scss/main.scss')
    .pipe(sass())
    .pipe(gulp.dest('app/css'));
  var skinimg = gulp.src('app/scss/skins/img/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(cache(imagemin()))
    .pipe(gulp.dest('app/css/img'))
  return merge(main, skinimg);
});

gulp.task('images', function(){
  var globalimages = gulp.src('app/images/**/*')
    .pipe(cache(imagemin()))
    .pipe(gulp.dest(dest + 'images'))
  var cssimg = gulp.src('app/css/img/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(cache(imagemin()))
    .pipe(gulp.dest(dest + 'css/img'))
  var skinimg = gulp.src('app/scss/skins/img/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(cache(imagemin()))
    .pipe(gulp.dest(dest + 'css/img'))
  return merge(globalimages, cssimg, skinimg);
});

gulp.task('clean:dist', function() {
  return del.sync(dest);
})

//Launch PHP server

gulp.task('connect-sync', function() {
  connect.server({
    port: '3005',
    base: '../../'
  }, function (){
    browserSync({
      proxy: 'http://univadislocal.prod.acquia-sites.com/cialis/edetail3/',
      browser: ["google chrome"]
    });
  });
 
  //Watcher
  gulp.watch('**/*.+(php|scss)', ['sass']).on('change', function () {
    browserSync.reload();
  });
});

//Watcher
gulp.task('watch', function(){
  // Everytime scss file is updated, it lauches sass task.
  gulp.watch('app/scss/*.scss', ['sass']);
  gulp.watch('app/inc/**/*.php', ['inc']); 
})

gulp.task('build', function (callback) {
  runSequence('clean:dist', 
    ['sass', 'useref', 'inc', 'images', 'fonts', 'audio'],
    callback
  )
})
gulp.task('default', function (callback) {
  runSequence(['sass', 'connect-sync'],
    callback
  )
})
